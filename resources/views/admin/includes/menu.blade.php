<nav class="sidebar sidebar-offcanvas" id="sidebar">
    <ul class="nav">
        <li class="nav-item">
            <a class="nav-link" href="{{ route('news.index') }}"
               aria-controls="news">
                <i class="mdi mdi-pencil menu-icon"></i>
                <span class="menu-title">News</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{ route('customers.index') }}"
               aria-controls="customers">
                <i class="mdi mdi-account-multiple menu-icon"></i>
                <span class="menu-title">Customers</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{ route('product.index') }}"
               aria-controls="product">
                <i class="mdi mdi-archive menu-icon"></i>
                <span class="menu-title">Products</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{ route('review.index') }}"
               aria-controls="review">
                <i class="mdi mdi-inbox menu-icon"></i>
                <span class="menu-title">Reviews</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{ route('contacts-email.index') }}"
               aria-controls="contacts-email">
                <i class="mdi mdi-contact-mail menu-icon"></i>
                <span class="menu-title">Contacts email</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{ route('contacts.index') }}"
               aria-controls="contacts">
                <i class="mdi mdi-account-box-outline menu-icon"></i>
                <span class="menu-title">Contacts</span>
            </a>
        </li>
    </ul>
</nav>
