@extends('admin.layouts.app')

@section('title')
    News edit
@stop

@section('css')
    <link rel="stylesheet" href="{{asset('vendors/summernote/dist/summernote-bs4.css')}}">
@stop

@section('content')
    <div class="main-panel">
        <div class="content-wrapper">
            <div class="row grid-margin">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">News edit</h4>
                            @if(session()->has('success'))
                                <div class="alert alert-success">
                                    {{ session()->get('success') }}
                                </div>
                            @endif
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            <ul class="nav nav-tabs" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="contact-tab" data-toggle="tab" href="#en"
                                       role="tab" aria-controls="en" aria-selected="true">
                                        <i class="flag-icon flag-icon-us"></i> English
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="home-tab" data-toggle="tab" href="#az"
                                       role="tab" aria-controls="az" aria-selected="false">
                                        <i class="flag-icon flag-icon-az"></i> Azərbaycanca
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#ru"
                                       role="tab" aria-controls="ru" aria-selected="false">
                                        <i class="flag-icon flag-icon-ru"></i> Русcкий
                                    </a>
                                </li>
                            </ul>
                            <form action="{{ route('news.update', $news->id) }}" method="POST">
                                @method('PATCH')
                                @csrf
                                <div class="tab-content">
                                    <div class="tab-pane fade" id="az" role="tabpanel"
                                         aria-labelledby="az">
                                        <div class="media">
                                            <div class="card-body">
                                                <div class="form-group">
                                                    <label for="title">Title</label>
                                                    <input type="text" value="{{ $news->title('az') }}" name="title_az"
                                                           class="form-control" id="title"
                                                           placeholder="Title">
                                                </div>
                                                <div class="form-group">
                                                    <label for="text">Short description</label>
                                                    <input type="text" value="{{ $news->short_desc('az') }}"
                                                           name="short_desc_az" class="form-control" id="text"
                                                           placeholder="Short description">
                                                </div>
                                                <div class="form-group">
                                                    <textarea id="summernote_az"
                                                              name="body_az">{{ $news->body('az') }}</textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="ru" role="tabpanel"
                                         aria-labelledby="ru">
                                        <div class="media">
                                            <div class="card-body">
                                                <div class="form-group">
                                                    <label for="title">Title</label>
                                                    <input type="text" value="{{ $news->title('ru') }}" name="title_ru"
                                                           class="form-control" id="title"
                                                           placeholder="Title">
                                                </div>
                                                <div class="form-group">
                                                    <label for="text">Short description</label>
                                                    <input type="text" value="{{ $news->short_desc('ru') }}"
                                                           name="short_desc_ru" class="form-control" id="text"
                                                           placeholder="Short description">
                                                </div>
                                                <div class="form-group">
                                                    <textarea id="summernote_ru"
                                                              name="body_ru">{{ $news->body('ru') }}</textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade show active" id="en" role="tabpanel"
                                         aria-labelledby="en">
                                        <div class="media">
                                            <div class="card-body">
                                                <div class="form-group">
                                                    <label for="title">Title</label>
                                                    <input type="text" value="{{ $news->title }}" name="title_en"
                                                           class="form-control" id="title"
                                                           placeholder="Title">
                                                </div>
                                                <div class="form-group">
                                                    <label for="text">Short description</label>
                                                    <input type="text" value="{{ $news->short_desc }}"
                                                           name="short_desc_en" class="form-control" id="text"
                                                           placeholder="Short description">
                                                </div>
                                                <div class="form-group">
                                                    <textarea id="summernote_en"
                                                              name="body_en">{{ $news->body }}</textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="form-check">
                                            <label class="form-check-label">
                                                <input type="checkbox" name="show_on_main"
                                                       class="form-check-input"
                                                       value="1"
                                                       @if($news->show_on_main == 1) checked @endif>
                                                Show on main
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="images">
                                            <div class="image-tile">
                                                <image-crud :image="{{ $news->newsImage }}" :product_id="{{ $news->id }}"></image-crud>
                                            </div>
                                        </div>
                                    </div>
                                    <button type="submit" class="btn btn-primary">Update</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('js/jquery-file-upload.js') }}"></script>
    <script src="{{ asset('vendors/summernote/dist/summernote-bs4.min.js') }}"></script>
    <script src="{{ asset('js/editorDemo.js') }}"></script>
    <script src="{{ asset('js/tabs.js') }}"></script>
    <script>
        $('input[type="checkbox"]').change(function(){
            this.value = (Number(this.checked));
        });
    </script>
@stop
