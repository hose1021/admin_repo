@extends('admin.layouts.app')

@section('title')
    News list
@stop

@section('content')
    <div class="main-panel">
        <div class="content-wrapper">
            <div class="row">
                <div class="col-12 grid-margin">
                    <div class="card">
                        <div class="card-body">
                            <div class="d-flex align-items-start justify-content-between">
                                <h4 class="card-title">News list</h4>
                                <a href="{{ route("news.create") }}" class="btn btn-success btn-sm">Create</a>
                            </div>
                            <div class="row">
                                <div class="table-sorter-wrapper col-lg-12 table-responsive">
                                    <table id="sortable-table-2" class="table table-striped">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th class="sortStyle">Tittle<i class="mdi mdi-chevron-down"></i>
                                            </th>
                                            <th>Short description</th>
                                            <th class="sortStyle">Published at<i class="mdi mdi-chevron-down"></i></th>
                                            <th class="sortStyle">Created at<i class="mdi mdi-chevron-down"></i></th>
                                            <th title="Show on main">Show on main</th>
                                            <th>Actions</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach ($news as $new_post)
                                            <tr>
                                                <td>{{ $new_post['id'] }}</td>
                                                <td class="lang_title">
                                                    <a href="#">{{ $new_post->title }}</a>
                                                </td>
                                                <td>{{ $new_post->short_desc }}</td>
                                                <td>{{ $new_post->published_at->format('Y-m-d H:i') }}</td>
                                                <td>{{ $new_post->created_at->toDateString() }}</td>
                                                <td style="text-align: center">
                                                    @if ($new_post->show_on_main == 1)
                                                        <div class="badge badge-success">On main</div>
                                                    @elseif($new_post->show_on_main ==0)
                                                        <div class="badge badge-danger">Not on main</div>
                                                    @endif
                                                </td>
                                                <td>
                                                    <a href="{{ route('news.edit',$new_post['id'])}}"
                                                       class="btn btn-outline-primary btn-sm">Edit</a>
                                                </td>
                                                <td>
                                                    <form action="{{ route('news.destroy', $new_post['id'])}}"
                                                          method="post">
                                                        @csrf
                                                        @method('DELETE')
                                                        @if ($new_post->status == '0')
                                                            <button class="btn btn-outline-success btn-sm"
                                                                    type="submit">
                                                                Activate
                                                            </button>
                                                        @elseif($new_post->status == '1')
                                                            <button class="btn btn-outline-danger btn-sm" type="submit">
                                                                Deactivate
                                                            </button>
                                                        @endif
                                                    </form>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                    {{ $news->links() }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:../../partials/_footer.html -->
        <!-- partial -->
    </div>
    <!-- main-panel ends -->
@endsection

@section('scripts')
    <script src="{{ asset('js/jq.tablesort.js') }}"></script>
    <script src="{{ asset('js/tablesorter.js') }}"></script>
@endsection
