@extends('admin.layouts.app')

@section('title')
    Create news
@stop

@section('css')
    <link rel="stylesheet" href="{{asset('vendors/summernote/dist/summernote-bs4.css')}}">
@stop

@section('content')
    <div class="main-panel">
        <div class="content-wrapper">
            <div class="row grid-margin">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="d-flex align-items-start justify-content-between">
                                <h4 class="card-title">News create</h4>
                                <a href="{{ route("news.index") }}" class="btn btn-primary btn-sm">Return to list</a>
                            </div>
                            @if(session()->has('success'))
                                <div class="alert alert-success">
                                    {{ session()->get('success') }}
                                </div>
                            @endif
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            <ul class="nav nav-tabs" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="contact-tab" data-toggle="tab" href="#en"
                                       role="tab" aria-controls="en" aria-selected="true">
                                        <i class="flag-icon flag-icon-us"></i> English
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="home-tab" data-toggle="tab" href="#az"
                                       role="tab" aria-controls="az" aria-selected="false">
                                        <i class="flag-icon flag-icon-az"></i> Azərbaycanca
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#ru"
                                       role="tab" aria-controls="ru" aria-selected="false">
                                        <i class="flag-icon flag-icon-ru"></i> Русcкий
                                    </a>
                                </li>
                            </ul>
                            <form action="{{ route('news.store') }}" enctype="multipart/form-data" method="POST">
                                @csrf
                                <div class="tab-content">
                                    <div class="tab-pane fade show active" id="en" role="tabpanel"
                                         aria-labelledby="en">
                                        <div class="media">
                                            <div class="tab-body">
                                                <div class="form-group">
                                                    <label for="title">Title</label>
                                                    <input type="text" value="{{Request::old('title_en')}}"
                                                           name="title_en"
                                                           class="form-control" id="title"
                                                           placeholder="Title">
                                                </div>
                                                <div class="form-group">
                                                    <label for="text">Short description</label>
                                                    <input type="text" value="{{Request::old('short_desc_en')}}"
                                                           name="short_desc_en" class="form-control" id="text"
                                                           placeholder="Short description">
                                                </div>
                                                <div class="form-group">
                                                <textarea id="summernote_en"
                                                          name="body_en">{{Request::old('body_en')}}</textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="az" role="tabpanel"
                                         aria-labelledby="az">
                                        <div class="media">
                                            <div class="tab-body">
                                                <div class="form-group">
                                                    <label for="title">Title</label>
                                                    <input type="text" value="{{Request::old('title_az')}}"
                                                           name="title_az"
                                                           class="form-control" id="title"
                                                           placeholder="Title">
                                                </div>
                                                <div class="form-group">
                                                    <label for="text">Short description</label>
                                                    <input type="text" value="{{Request::old('short_desc_az')}}"
                                                           name="short_desc_az" class="form-control" id="text"
                                                           placeholder="Short description">
                                                </div>
                                                <div class="form-group">
                                                <textarea id="summernote_az"
                                                          name="body_az">{{Request::old('body_az')}}</textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="ru" role="tabpanel"
                                         aria-labelledby="ru">
                                        <div class="media">
                                            <div class="tab-body">
                                                <div class="form-group">
                                                    <label for="title">Title</label>
                                                    <input type="text" value="{{Request::old('title_ru')}}"
                                                           name="title_ru"
                                                           class="form-control" id="title"
                                                           placeholder="Title">
                                                </div>
                                                <div class="form-group">
                                                    <label for="text">Short description</label>
                                                    <input type="text" value="{{Request::old('short_desc_ru')}}"
                                                           name="short_desc_ru" class="form-control" id="text"
                                                           placeholder="Short description">
                                                </div>
                                                <div class="form-group">
                                                <textarea id="summernote_ru"
                                                          name="body_ru">{{Request::old('body_ru')}}</textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="form-check">
                                        <label class="form-check-label">
                                            <input type="checkbox" name="show_on_main"
                                                   class="form-check-input">
                                            Show on main
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Images upload</label>
                                    <input style="display: block" type='file' id="image" name="image[]"
                                           accept=".png, .jpg, .jpeg"
                                           multiple/>
                                </div>
                                <button type="submit" class="btn btn-primary">Create</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('js/jquery-file-upload.js') }}"></script>
    <script src="{{ asset('vendors/summernote/dist/summernote-bs4.min.js') }}"></script>
    <script src="{{ asset('js/editorDemo.js') }}"></script>
    <script src="{{ asset('js/tabs.js') }}"></script>
    <script>
        $('input[type="checkbox"]').change(function(){
            this.value = (Number(this.checked));
        });
    </script>
@stop
