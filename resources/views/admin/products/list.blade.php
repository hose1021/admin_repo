@extends('admin.layouts.app')

@section('title')
    Products list
@stop

@section('content')
    <div class="main-panel">
        <div class="content-wrapper">
            <div class="row">
                <div class="col-12 grid-margin">
                    <div class="card">
                        <div class="card-body">
                            <div class="d-flex align-items-start justify-content-between">
                                <h4 class="card-title">Products list</h4>
                                <a href="{{ route("product.create") }}" class="btn btn-success btn-sm">Create</a>
                            </div>
                            <div class="row">
                                <div class="table-sorter-wrapper col-lg-12 table-responsive">
                                    <table id="sortable-table-2" class="table table-striped">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th class="sortStyle">Tittle<i class="mdi mdi-chevron-down"></i>
                                            </th>
                                            <th>Image</th>
                                            <th>Short description</th>
                                            <th class="sortStyle">Created at<i class="mdi mdi-chevron-down"></i></th>
                                            <th>Actions</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach ($products as $product)
                                            <tr>
                                                <td>{{ $product['id'] }}</td>
                                                <td class="lang_title">
                                                    <a href="#">{{ $product->name }}</a>
                                                </td>
                                                <td>
                                                    @if ($product->productMainImage)
                                                        <img src="{{ asset('media/product/' . $product->productMainImage->file_name) }}" alt="">
                                                    @endif
                                                </td>
                                                <td>{{ $product->short_desc }}</td>
                                                <td>{{ $product->created_at->toDateString() }}</td>
                                                <td>
                                                    <a href="{{ route('product.edit',$product['id'])}}"
                                                       class="btn btn-outline-primary btn-sm">Edit</a>
                                                </td>
                                                <td>
                                                    <form action="{{ route('product.destroy', $product['id'])}}"
                                                          method="post">
                                                        @csrf
                                                        @method('DELETE')
                                                        @if ($product->status == '0')
                                                            <button class="btn btn-outline-success btn-sm"
                                                                    type="submit">
                                                                Activate
                                                            </button>
                                                        @elseif($product->status == '1')
                                                            <button class="btn btn-outline-danger btn-sm" type="submit">
                                                                Deactivate
                                                            </button>
                                                        @endif
                                                    </form>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                    {{ $products->links() }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:../../partials/_footer.html -->
        <!-- partial -->
    </div>
    <!-- main-panel ends -->
@endsection

@section('scripts')
    <script src="{{ asset('js/jq.tablesort.js') }}"></script>
    <script src="{{ asset('js/tablesorter.js') }}"></script>
@endsection
