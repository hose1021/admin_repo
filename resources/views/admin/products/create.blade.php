@extends('admin.layouts.app')

@section('title')
    Create product
@stop

@section('css')
    <link rel="stylesheet" href="{{asset('admin_assets/vendors/summernote/dist/summernote-bs4.css')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.0/min/dropzone.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.0/dropzone.js"></script>
@stop

@section('content')
    <div class="main-panel" id="app">
        <div class="content-wrapper">
            <div class="row grid-margin">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="d-flex align-items-start justify-content-between">
                                <h4 class="card-title">Product create</h4>
                                <a href="{{ route("product.index") }}" class="btn btn-primary btn-sm">Return to list</a>
                            </div>
                            @if(session()->has('success'))
                                <div class="alert alert-success">
                                    {{ session()->get('success') }}
                                </div>
                            @endif
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                            <form action="{{ route('product.store') }}" enctype="multipart/form-data" method="POST">
                                @csrf
                                <div class="form-group">
                                    <label>Main image upload</label>
                                    <input style="display: block" type='file' id="main_image" name="main_image"
                                           accept=".png, .jpg, .jpeg"
                                    />
                                </div>
                                <ul class="nav nav-tabs" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" id="contact-tab" data-toggle="tab" href="#en"
                                           role="tab" aria-controls="en" aria-selected="true">
                                            <i class="flag-icon flag-icon-us"></i> English
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="home-tab" data-toggle="tab" href="#az"
                                           role="tab" aria-controls="az" aria-selected="false">
                                            <i class="flag-icon flag-icon-az"></i> Azərbaycanca
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="profile-tab" data-toggle="tab" href="#ru"
                                           role="tab" aria-controls="ru" aria-selected="false">
                                            <i class="flag-icon flag-icon-ru"></i> Русcкий
                                        </a>
                                    </li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane fade show active" id="en" role="tabpanel"
                                         aria-labelledby="en">
                                        <div class="media">
                                            <div class="tab-body">
                                                <div class="form-group">
                                                    <label for="title">Title</label>
                                                    <input type="text" value="{{Request::old('title_en')}}"
                                                           name="title_en"
                                                           class="form-control" id="title"
                                                           placeholder="Title">
                                                </div>
                                                <div class="form-group">
                                                    <label for="text">Short description</label>
                                                    <input type="text" value="{{Request::old('short_desc_en')}}"
                                                           name="short_desc_en" class="form-control" id="text"
                                                           placeholder="Short description">
                                                </div>
                                                <div class="form-group">
                                                <textarea id="summernote_en"
                                                          name="body_en">{{Request::old('body_en')}}</textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="az" role="tabpanel"
                                         aria-labelledby="az">
                                        <div class="media">
                                            <div class="tab-body">
                                                <div class="form-group">
                                                    <label for="title">Title</label>
                                                    <input type="text" value="{{Request::old('title_az')}}"
                                                           name="title_az"
                                                           class="form-control" id="title"
                                                           placeholder="Title">
                                                </div>
                                                <div class="form-group">
                                                    <label for="text">Short description</label>
                                                    <input type="text" value="{{Request::old('short_desc_az')}}"
                                                           name="short_desc_az" class="form-control" id="text"
                                                           placeholder="Short description">
                                                </div>
                                                <div class="form-group">
                                                <textarea id="summernote_az"
                                                          name="body_az">{{Request::old('body_az')}}</textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="ru" role="tabpanel"
                                         aria-labelledby="ru">
                                        <div class="media">
                                            <div class="tab-body">
                                                <div class="form-group">
                                                    <label for="title">Title</label>
                                                    <input type="text" value="{{Request::old('title_ru')}}"
                                                           name="title_ru"
                                                           class="form-control" id="title"
                                                           placeholder="Title">
                                                </div>
                                                <div class="form-group">
                                                    <label for="text">Short description</label>
                                                    <input type="text" value="{{Request::old('short_desc_ru')}}"
                                                           name="short_desc_ru" class="form-control" id="text"
                                                           placeholder="Short description">
                                                </div>
                                                <div class="form-group">
                                                <textarea id="summernote_ru"
                                                          name="body_ru">{{Request::old('body_ru')}}</textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="container">
                                    <h2>Laravel 6 Upload Image Using Dropzone Tutorial</h2><br/>
                                    <div id="dZUpload" class="dropzone">
                                        <div class="dz-default dz-message"></div>
                                    </div>
                                </div>
                                {{--                                <div class="form-group">--}}
                                {{--                                    <label>Images upload</label>--}}
                                {{--                                    <input style="display: block" type='file' id="image" name="image[]"--}}
                                {{--                                           accept=".png, .jpg, .jpeg"--}}
                                {{--                                           multiple/>--}}
                                {{--                                </div>--}}
                                <button type="submit" class="btn btn-primary">Create</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('admin_assets/js/jquery-file-upload.js') }}"></script>
    <script src="{{ asset('admin_assets/vendors/summernote/dist/summernote-bs4.min.js') }}"></script>
    <script src="{{ asset('admin_assets/js/editorDemo.js') }}"></script>
    <script src="{{ asset('admin_assets/js/tabs.js') }}"></script>
    <script>
        $('.form-check-input').click(function () {
            $(this.form.elements).filter(':checkbox').prop('checked', this.checked);
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            Dropzone.autoDiscover = false;
            $("#dZUpload").dropzone({
                url: "hn_SimpeFileUploader.ashx",
                addRemoveLinks: true,
                success: function (file, response) {
                    var imgName = response;
                    file.previewElement.classList.add("dz-success");
                    console.log("Successfully uploaded :" + imgName);
                },
                error: function (file, response) {
                    file.previewElement.classList.add("dz-error");
                }
            });
        });

    </script>
@stop
