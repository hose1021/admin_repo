<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title', '') | Panda</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <script src="{{ asset('admin_assets/js/app.js') }}"></script>
    <!-- Styles -->
{{--    <link href="{{ asset('admin_assets/css/app.css') }}" rel="stylesheet">--}}
    <link rel="stylesheet" href="{{ asset('admin_assets/vendors/iconfonts/mdi/font/css/materialdesignicons.min.css') }}">
    <link rel="stylesheet" href="{{ asset('admin_assets/vendors/css/vendor.bundle.base.css') }}">
    <link rel="stylesheet" href="{{ asset('admin_assets/vendors/iconfonts/simple-line-icon/css/simple-line-icons.css') }}">
    <link rel="stylesheet" href="{{ asset('admin_assets/vendors/css/vendor.bundle.addons.css') }}">
    <link rel="stylesheet" href="{{ asset('admin_assets/vendors/iconfonts/font-awesome/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('admin_assets/vendors/iconfonts/flag-icon-css/css/flag-icon.min.css') }}" />
    @yield('css')

    <!-- inject:css -->
    <link rel="stylesheet" href="{{ asset('admin_assets/css/vertical-layout-light/style.css') }}">
    <!-- endinject -->
    <link rel="shortcut icon" href="{{ asset('admin_assets/images/favicon.png') }}"/>
</head>
<body class="sidebar-dark">
<div id="app">
    <div class="container-scroller">
        @include('admin.includes.header')
        <div class="container-fluid page-body-wrapper">
            @include('admin.includes.menu')
            @yield('content')
        </div>
    </div>
</div>
</body>
<!-- plugins:js -->
<script src="{{ asset('admin_assets/vendors/js/vendor.bundle.base.js') }}"></script>
<script src="{{ asset('admin_assets/vendors/js/vendor.bundle.addons.js') }}"></script>
<script src="{{ asset('admin_assets/js/app.js') }}"></script>
<!-- endinject -->
<!-- Plugin js for this page-->
<!-- End plugin js for this page-->
<!-- inject:js -->
<script src="{{ asset('admin_assets/js/off-canvas.js') }}"></script>
<script src="{{ asset('admin_assets/js/hoverable-collapse.js') }}"></script>
<script src="{{ asset('admin_assets/js/template.js') }}"></script>
<script src="{{ asset('admin_assets/js/settings.js') }}"></script>
<script src="{{ asset('admin_assets/js/todolist.js') }}"></script>
<!-- endinject -->
<!-- Custom js for this page-->
@yield('scripts')
<!-- End custom js for this page-->
</html>
