@extends('admin.layouts.app')

@section('title')
    Create review
@stop

{{--TODO edit all names--}}

@section('css')
    <link rel="stylesheet" href="{{asset('admin_assets/vendors/summernote/dist/summernote-bs4.css')}}">
@stop

@section('content')
    <div class="main-panel">
        <div class="content-wrapper">
            <div class="row grid-margin">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="d-flex align-items-start justify-content-between">
                                <h4 class="card-title">Review create</h4>
                                <a href="{{ route("review.index") }}" class="btn btn-primary btn-sm">Return to list</a>
                            </div>
                            @if(session()->has('success'))
                                <div class="alert alert-success">
                                    {{ session()->get('success') }}
                                </div>
                            @endif
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            <ul class="nav nav-tabs" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="contact-tab" data-toggle="tab" href="#en"
                                       role="tab" aria-controls="en" aria-selected="true">
                                        <i class="flag-icon flag-icon-us"></i> English
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="home-tab" data-toggle="tab" href="#az"
                                       role="tab" aria-controls="az" aria-selected="false">
                                        <i class="flag-icon flag-icon-az"></i> Azərbaycanca
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#ru"
                                       role="tab" aria-controls="ru" aria-selected="false">
                                        <i class="flag-icon flag-icon-ru"></i> Русcкий
                                    </a>
                                </li>
                            </ul>
                            <form action="{{ route('review.store') }}" method="POST">
                                @csrf
                                <div class="tab-content">
                                    <div class="tab-pane fade" id="az" role="tabpanel"
                                         aria-labelledby="az">
                                        <div class="media">
                                            <div class="card-body">
                                                <div class="form-group">
                                                    <label for="title">Designator</label>
                                                    <input type="text" value="{{ Request::old('designator_az') }}"
                                                           name="designator_az"
                                                           class="form-control" id="title"
                                                           placeholder="Title">
                                                </div>
                                                <div class="form-group">
                                                    <label for="text">Position</label>
                                                    <input type="text" value="{{ Request::old('position_az') }}"
                                                           name="position_az" class="form-control" id="text"
                                                           placeholder="Short description">
                                                </div>
                                                <div class="form-group">
                                                <textarea id="summernote_az"
                                                          name="review_az">{{ Request::old('review_az') }}</textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="ru" role="tabpanel"
                                         aria-labelledby="ru">
                                        <div class="media">
                                            <div class="card-body">
                                                <div class="form-group">
                                                    <label for="title">Designator</label>
                                                    <input type="text" value="{{ Request::old('designator_ru') }}"
                                                           name="designator_ru"
                                                           class="form-control" id="title"
                                                           placeholder="Title">
                                                </div>
                                                <div class="form-group">
                                                    <label for="text">Position</label>
                                                    <input type="text" value="{{ Request::old('position_ru') }}"
                                                           name="position_ru" class="form-control" id="text"
                                                           placeholder="Short description">
                                                </div>
                                                <div class="form-group">
                                                <textarea id="summernote_ru"
                                                          name="review_ru">{{ Request::old('review_ru') }}</textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade show active" id="en" role="tabpanel"
                                         aria-labelledby="en">
                                        <div class="media">
                                            <div class="card-body">
                                                <div class="form-group">
                                                    <label for="title">Designator</label>
                                                    <input type="text" value="{{ Request::old('designator_en') }}"
                                                           name="designator_en"
                                                           class="form-control" id="title"
                                                           placeholder="Title">
                                                </div>
                                                <div class="form-group">
                                                    <label for="text">Position</label>
                                                    <input type="text" value="{{ Request::old('position_en') }}"
                                                           name="position_en" class="form-control" id="text"
                                                           placeholder="Short description">
                                                </div>
                                                <div class="form-group">
                                                <textarea id="summernote_en"
                                                          name="review_en">{{ Request::old('review_en') }}</textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col">
                                        <label>Date time:</label>
                                        <input class="form-control" name="datetime" data-inputmask="'alias': 'datetime'"
                                               value="{{ Request::old('datetime') }}"/>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-primary">Create</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('admin_assets/js/jquery-file-upload.js') }}"></script>
    <script src="{{ asset('admin_assets/vendors/summernote/dist/summernote-bs4.min.js') }}"></script>
    <script src="{{ asset('admin_assets/js/editorDemo.js') }}"></script>
    <script src="{{ asset('admin_assets/js/tabs.js') }}"></script>
    <script>
        $('.form-check-input').click(function () {
            $(this.form.elements).filter(':checkbox').prop('checked', this.checked);
        });
    </script>
@stop
