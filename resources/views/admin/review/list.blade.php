@extends('admin.layouts.app')

@section('title')
    Reviews list
@stop

@section('content')
    <div class="main-panel">
        <div class="content-wrapper">
            <div class="row">
                <div class="col-12 grid-margin">
                    <div class="card">
                        <div class="card-body">
                            <div class="d-flex align-items-start justify-content-between">
                                <h4 class="card-title">Reviews list</h4>
                                <a href="{{ route("review.create") }}" class="btn btn-success btn-sm">Create</a>
                            </div>
                            <div class="row">
                                <div class="table-sorter-wrapper col-lg-12 table-responsive">
                                    <table id="sortable-table-2" class="table table-striped">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Designator</th>
                                            <th>Position</th>
                                            <th>Review</th>
                                            <th class="sortStyle" title="1 if published | 0 if deleted">Data time<i
                                                    class="mdi mdi-chevron-down"></i></th>
                                            <th>Actions</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach ($reviews as $review)
                                            <tr>
                                                <td>{{ $review->id }}</td>
                                                <td>{{ $review->designator }}</td>
                                                <td>{{ $review->position }}</td>
                                                <td>{{ $review->review }}</td>
                                                <td>{{ $review->datetime }}</td>
                                                <td>
                                                    <a href="{{ route('review.edit',$review['id'])}}"
                                                       class="btn btn-primary btn-sm">Edit</a>
                                                </td>
                                                <td>
                                                    <form action="{{ route('review.destroy', $review['id'])}}"
                                                          method="post">
                                                        @csrf
                                                        @method('DELETE')
                                                        @if ($review->status == '0')
                                                            <button class="btn btn-success btn-sm"
                                                                    type="submit">
                                                                Activate
                                                            </button>
                                                        @elseif($review->status == '1')
                                                            <button class="btn btn-outline-danger btn-sm" type="submit">
                                                                Deactivate
                                                            </button>
                                                        @endif
                                                    </form>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                    {{ $reviews->links() }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:../../partials/_footer.html -->
        <!-- partial -->
    </div>
    <!-- main-panel ends -->
@endsection

@section('scripts')
    <script src="{{ asset('js/jq.tablesort.js') }}"></script>
    <script src="{{ asset('js/tablesorter.js') }}"></script>
@endsection
