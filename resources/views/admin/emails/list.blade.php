@extends('admin.layouts.app')

@section('title')
    Contacts email list
@stop

@section('content')
    <div class="main-panel">
        <div class="content-wrapper">
            <div class="row">
                <div class="col-12 grid-margin">
                    <div class="card">
                        <div class="card-body">
                            <div class="d-flex align-items-start justify-content-between">
                                <h4 class="card-title">Contacts email list</h4>
                                <a href="{{ route("contacts-email.create") }}" class="btn btn-success btn-sm">Create</a>
                            </div>
                            <div class="row">
                                <div class="table-sorter-wrapper col-lg-12 table-responsive">
                                    <table id="sortable-table-2" class="table table-striped">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th class="sortStyle">Name<i class="mdi mdi-chevron-down"></i>
                                            </th>
                                            <th class="sortStyle">Data time<i class="mdi mdi-chevron-down"></i></th>
                                            <th title="Show on main">Created at</th>
                                            <th>Actions</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach ($emails as $email)
                                            <tr>
                                                <td>{{ $email['id'] }}</td>
                                                <td class="lang_title">
                                                    <a href="#">{{ $email->name }}</a>
                                                </td>
                                                <td>{{ $email->datetime }}</td>
                                                <td>{{ $email->created_at->toDateString() }}</td>
                                                <td>
                                                    <a href="{{ route('contacts-email.edit',$email['id'])}}"
                                                       class="btn btn-primary btn-sm">Edit</a>
                                                </td>
                                                <td>
                                                    <form action="{{ route('contacts-email.destroy', $email['id'])}}"
                                                          method="post">
                                                        @csrf
                                                        @method('DELETE')
                                                        @if ($email->status == '0')
                                                            <button class="btn btn-success btn-sm"
                                                                    type="submit">
                                                                Activate
                                                            </button>
                                                        @elseif($email->status == '1')
                                                            <button class="btn btn-outline-danger btn-sm" type="submit">
                                                                Deactivate
                                                            </button>
                                                        @endif
                                                    </form>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                    {{ $emails->links() }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:../../partials/_footer.html -->
        <!-- partial -->
    </div>
    <!-- main-panel ends -->
@endsection

@section('scripts')
    <script src="{{ asset('js/jq.tablesort.js') }}"></script>
    <script src="{{ asset('js/tablesorter.js') }}"></script>
@endsection
