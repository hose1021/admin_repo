@extends('admin.layouts.app')

@section('title')
    Contacts email edit
@stop

@section('css')
    <link rel="stylesheet" href="{{asset('vendors/summernote/dist/summernote-bs4.css')}}">
@stop

@section('content')
    <div class="main-panel">
        <div class="content-wrapper">
            <div class="row grid-margin">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Contacts email edit</h4>
                            @if(session()->has('success'))
                                <div class="alert alert-success">
                                    {{ session()->get('success') }}
                                </div>
                            @endif
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            <ul class="nav nav-tabs" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="contact-tab" data-toggle="tab" href="#en"
                                       role="tab" aria-controls="en" aria-selected="true">
                                        <i class="flag-icon flag-icon-us"></i> English
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="home-tab" data-toggle="tab" href="#az"
                                       role="tab" aria-controls="az" aria-selected="false">
                                        <i class="flag-icon flag-icon-az"></i> Azərbaycanca
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#ru"
                                       role="tab" aria-controls="ru" aria-selected="false">
                                        <i class="flag-icon flag-icon-ru"></i> Русcкий
                                    </a>
                                </li>
                            </ul>
                            <form action="{{ route('contacts-email.update', $emails->id) }}" method="POST">
                                @method('PATCH')
                                @csrf
                                <div class="tab-content">
                                    <div class="tab-pane fade" id="az" role="tabpanel"
                                         aria-labelledby="az">
                                        <div class="media">
                                            <div class="card-body">
                                                <div class="form-group">
                                                    <label for="title">Name</label>
                                                    <input type="text" value="{{ $emails->name('az') }}" name="name_az"
                                                           class="form-control" id="name"
                                                           placeholder="Name">
                                                </div>
                                                <div class="form-group">
                                                    <label for="text">Short description</label>
                                                    <input type="text" value="{{ $emails->email('az') }}"
                                                           name="email_az" class="form-control" id="text"
                                                           placeholder="Email">
                                                </div>
                                                <div class="form-group">
                                                    <textarea id="summernote_az"
                                                              name="message_az">{{ $emails->message('az') }}</textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="ru" role="tabpanel"
                                         aria-labelledby="ru">
                                        <div class="media">
                                            <div class="card-body">
                                                <div class="form-group">
                                                    <label for="title">Name</label>
                                                    <input type="text" value="{{ $emails->name('ru') }}" name="name_ru"
                                                           class="form-control" id="name"
                                                           placeholder="Name">
                                                </div>
                                                <div class="form-group">
                                                    <label for="text">Short description</label>
                                                    <input type="text" value="{{ $emails->email('ru') }}"
                                                           name="email_ru" class="form-control" id="text"
                                                           placeholder="Email">
                                                </div>
                                                <div class="form-group">
                                                    <textarea id="summernote_ru"
                                                              name="message_ru">{{ $emails->message('ru') }}</textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade show active" id="en" role="tabpanel"
                                         aria-labelledby="en">
                                        <div class="media">
                                            <div class="card-body">
                                                <div class="form-group">
                                                    <label for="title">Name</label>
                                                    <input type="text" value="{{ $emails->name }}" name="name_en"
                                                           class="form-control" id="name"
                                                           placeholder="Name">
                                                </div>
                                                <div class="form-group">
                                                    <label for="text">Short description</label>
                                                    <input type="text" value="{{ $emails->email }}"
                                                           name="email_en" class="form-control" id="text"
                                                           placeholder="Email">
                                                </div>
                                                <div class="form-group">
                                                    <textarea id="summernote_en"
                                                              name="message_en">{{ $emails->message }}</textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col">
                                            <label>Date time:</label>
                                            <input class="form-control" name="datetime" data-inputmask="'alias': 'datetime'" value="{{ $emails->datetime }}"/>
                                        </div>
                                    </div>
                                    <button type="submit" class="btn btn-primary">Update</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('js/jquery-file-upload.js') }}"></script>
    <script src="{{ asset('vendors/summernote/dist/summernote-bs4.min.js') }}"></script>
    <script src="{{ asset('js/editorDemo.js') }}"></script>
    <script src="{{ asset('js/tabs.js') }}"></script>
    <script>
        $('.form-check-input').click(function () {
            $(this.form.elements).filter(':checkbox').prop('checked', this.checked);
        });
    </script>
@stop
