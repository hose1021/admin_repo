@extends('admin.layouts.app')

@section('title')
    Customers list
@stop

@section('content')
    <div class="main-panel">
        <div class="content-wrapper">
            <div class="row">
                <div class="col-12 grid-margin">
                    <div class="card">
                        <div class="card-body">
                            <div class="d-flex align-items-start justify-content-between">
                                <h4 class="card-title">Customers list</h4>
                                <a href="{{ route("customers.create") }}" class="btn btn-success btn-sm">Create</a>
                            </div>
                            <div class="row">
                                <div class="table-sorter-wrapper col-lg-12 table-responsive">
                                    <table id="sortable-table-2" class="table table-striped">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th class="sortStyle">Name<i class="mdi mdi-chevron-down"></i></th>
                                            <th>Description</th>
                                            <th>Latitude</th>
                                            <th>Longitude</th>
                                            <th class="sortStyle">Updated at<i class="mdi mdi-chevron-down"></i></th>
                                            <th>Actions</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach ($customers as $customer)
                                            <tr>
                                                <td>{{ $customer['id'] }}</td>
                                                <td><a href="#">{{ $customer->name }}</a></td>
                                                <td>{{ $customer->description }}</td>
                                                <td>{{ $customer->latitude }}</td>
                                                <td>{{ $customer->longitude }}</td>
                                                <td>{{ $customer->updated_at->toDateString() }}</td>
                                                <td>
                                                    <a href="{{ route('customers.edit',$customer['id'])}}"
                                                       class="btn btn-primary btn-sm">Edit</a>
                                                </td>
                                                <td>
                                                    <form action="{{ route('customers.destroy', $customer['id'])}}"
                                                          method="post">
                                                        @csrf
                                                        @method('DELETE')
                                                        @if ($customer->status == '0')
                                                            <button class="btn btn-success btn-sm"
                                                                    type="submit">
                                                                Activate
                                                            </button>
                                                        @elseif($customer->status == '1')
                                                            <button class="btn btn-outline-danger btn-sm" type="submit">
                                                                Deactivate
                                                            </button>
                                                        @endif
                                                    </form>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                    {{ $customers->links() }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:../../partials/_footer.html -->
        <!-- partial -->
    </div>
    <!-- main-panel ends -->
@endsection

@section('scripts')
    <script src="{{ asset('js/jq.tablesort.js') }}"></script>
    <script src="{{ asset('js/tablesorter.js') }}"></script>
@endsection
