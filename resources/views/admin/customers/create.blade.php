@extends('admin.layouts.app')

@section('title')
    Create customers
@stop

@section('css')
    <link rel="stylesheet" href="{{asset('vendors/summernote/dist/summernote-bs4.css')}}">
@stop

@section('content')
    <div class="main-panel">
        <div class="content-wrapper">
            <div class="row grid-margin">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="d-flex align-items-start justify-content-between">
                                <h4 class="card-title">Customers create</h4>
                                <a href="{{ route("customers.index") }}" class="btn btn-primary btn-sm">Return to
                                    list</a>
                            </div>
                            @if(session()->has('success'))
                                <div class="alert alert-success">
                                    {{ session()->get('success') }}
                                </div>
                            @endif
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            <form action="{{ route('customers.store') }}" enctype="multipart/form-data" method="POST">
                                @csrf
                                <div class="media">
                                    <div class="card-body">
                                        <div class="form-group">
                                            <label for="name">Title</label>
                                            <input type="text" value="{{Request::old('name')}}"
                                                   name="name"
                                                   class="form-control" id="name"
                                                   placeholder="Name">
                                        </div>
                                        <div class="form-group">
                                            <label for="latitude">Latitude</label>
                                            <input type="number" step="any" value="{{Request::old('latitude')}}"
                                                   name="latitude" class="form-control" id="latitude">
                                        </div>
                                        <div class="form-group">
                                            <label for="longitude">Longitude</label>
                                            <input type="number" step="any" value="{{Request::old('longitude')}}"
                                                   name="longitude" class="form-control" id="longitude">
                                        </div>
                                        <div class="form-group">
                                            <textarea id="description" name="body"></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label>List of related products</label>
                                            <select class="js-example-basic-multiple" name="products[]" multiple="multiple"
                                                    style="width:100%">
                                                @foreach ($products as $product)
                                                    <option value="{{ $product->id }}">
                                                        {{ $product->name }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <button type="submit" class="btn btn-primary">Create</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('js/jquery-file-upload.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>
    <script src="{{ asset('vendors/summernote/dist/summernote-bs4.min.js') }}"></script>
    <script src="{{ asset('js/editorDemo.js') }}"></script>
    <script src="{{ asset('js/tabs.js') }}"></script>
    <script>
        $('.form-check-input').click(function () {
            $(this.form.elements).filter(':checkbox').prop('checked', this.checked);
        });
    </script>
    <script src="{{ asset('js/select2.js') }}"></script>
@stop
