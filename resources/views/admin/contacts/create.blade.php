@extends('admin.layouts.app')

@section('title')
    Contacts create
@stop

@section('css')
    <link rel="stylesheet" href="{{asset('vendors/summernote/dist/summernote-bs4.css')}}">
@stop

@section('content')
    <div class="main-panel">
        <div class="content-wrapper">
            <div class="row grid-margin">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="d-flex align-items-start justify-content-between">
                                <h4 class="card-title">Contacts create</h4>
                                <a href="{{ route("contacts.index") }}" class="btn btn-primary btn-sm">Return to
                                    list</a>
                            </div>
                            @if(session()->has('success'))
                                <div class="alert alert-success">
                                    {{ session()->get('success') }}
                                </div>
                            @endif
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            <form action="{{ route('contacts.store') }}"
                                  method="POST">
                                @csrf
                                <div class="form-group ">
                                    <div class="form-group">
                                        <label for="address">Address</label>
                                        <textarea id="summernote_address"
                                                  name="address">{{Request::old('address')}}</textarea>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <div class="form-group">
                                        <label for="phone">Phone</label>
                                        <textarea id="summernote_phone"
                                                  name="phone">{{Request::old('phone')}}</textarea>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <div class="form-group">
                                        <label for="fax">Fax</label>
                                        <textarea id="summernote_fax"
                                                  name="fax">{{Request::old('fax')}}</textarea>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <div class="form-group">
                                        <label for="emails">Emails</label>
                                        <textarea id="summernote_emails"
                                                  name="emails">{{Request::old('emails')}}</textarea>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-primary">Create</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('js/jquery-file-upload.js') }}"></script>
    <script src="{{ asset('vendors/summernote/dist/summernote-bs4.min.js') }}"></script>
    <script src="{{ asset('js/editorDemo.js') }}"></script>
    <script src="{{ asset('js/tabs.js') }}"></script>
    <script>
        $('.form-check-input').click(function () {
            $(this.form.elements).filter(':checkbox').prop('checked', this.checked);
        });
    </script>
@stop
