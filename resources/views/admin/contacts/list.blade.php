@extends('admin.layouts.app')

@section('title')
    Contacts list
@stop

@section('content')
    <div class="main-panel">
        <div class="content-wrapper">
            <div class="row">
                <div class="col-12 grid-margin">
                    <div class="card">
                        <div class="card-body">
                            <div class="d-flex align-items-start justify-content-between">
                                <h4 class="card-title">Contacts list</h4>
                                <a href="{{ route("contacts.create") }}" class="btn btn-success btn-sm">Create</a>
                            </div>
                            <div class="row">
                                <div class="table-sorter-wrapper col-lg-12 table-responsive">
                                    <table id="sortable-table-2" class="table table-striped">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th class="sortStyle">Name<i class="mdi mdi-chevron-down"></i></th>
                                            <th>Created at</th>
                                            <th>Updated at</th>
                                            <th>Actions</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach ($contacts as $contact)
                                            <tr>
                                                <td>{{ $contact['id'] }}</td>
                                                <td class="lang_title">
                                                    <a href="#">{!! $contact->address !!}</a>
                                                </td>
                                                <td>{{ $contact->created_at->toDateString() }}</td>
                                                <td>{{ $contact->updated_at->toDateString() }}</td>
                                                <td>
                                                    <a href="{{ route('contacts.edit',$contact['id'])}}"
                                                       class="btn btn-primary btn-sm">Edit</a>
                                                </td>
                                                <td>
                                                    <form action="{{ route('contacts.destroy', $contact['id'])}}"
                                                          method="post">
                                                        @csrf
                                                        @method('DELETE')
                                                        @if ($contact->status == '0')
                                                            <button class="btn btn-success btn-sm"
                                                                    type="submit">
                                                                Activate
                                                            </button>
                                                        @elseif($contact->status == '1')
                                                            <button class="btn btn-outline-danger btn-sm" type="submit">
                                                                Deactivate
                                                            </button>
                                                        @endif
                                                    </form>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                    {{ $contacts->links() }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:../../partials/_footer.html -->
        <!-- partial -->
    </div>
    <!-- main-panel ends -->
@endsection

@section('scripts')
    <script src="{{ asset('js/jq.tablesort.js') }}"></script>
    <script src="{{ asset('js/tablesorter.js') }}"></script>
@endsection
