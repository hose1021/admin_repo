@extends('layout.panda-main')

@section('title')
    Main
@stop

@section('header')
    @include('include.header_buttons')
@endsection

@section('content')
    <section class="main news">
        <div class="container">
            <p class="caption grey-clr-txt">News</p>
            <div class="caption_line grey-clr-bg"></div>
            <div class="col-md-10 mr-md-auto ml-md-auto">
                <div class="slider demo news_slider">
                    @foreach($data['lastNews'] as $news)
                        <div class="card border-0">
                            <div class="news_animation_block">
                                <img class="news_slider_img wow fadeInLeft" src="{{ asset('image/news_image.png') }}"
                                     alt="">
                                <a href="#" class="news_link_icon">
                                    <img src="{{ asset('image/icon/little_panda_icon.svg') }}" alt="">
                                </a>
                                <a href="#" class="news_link">read more</a>
                            </div>
                            <div class="card-body pl-0">
                                <p class="card-title sf-500-font grey-clr-txt">
                                    {{ $news->title }}</p>
                                <div class="card-text sf-500-font light-grey-clr-txt">
                                    <i class="far fa-calendar light-grey-clr-txt"></i>
                                    <span class="grey-color" style="padding-left: 18px">
                                    {{ $news->created_at->format('M d, Y') }}
                                </span>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>

        </div>
        </div>
    </section>
    <section class="main clients white-grey-clr-bg">
        <div class="container wow slideInUp" data-wow-offset="100">
            <p class="caption grey-clr-txt">Customers</p>
            <div class="caption_line grey-clr-bg"></div>

            <div class="col-md-10 m-auto">
                <div class="d-flex flex-wrap justify-content-between text-center m-b-40">
                    @foreach ($data['customers'] as $customer)
                        <div class="col-md-3">
                            <img src="{{ asset('image/company_1_logo.png') }}" class="clients_icon">
                            <div>
                                <p class="text-center sf-400-font grey-clr-txt">
                                    {!! $customer->description !!}
                                </p>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="col-md-3 m-auto">
                <a class="col-md-12 btn btn-black-border" href="">
                    view customers</a>
            </div>
        </div>
    </section>
    <section class="main reviews">
        <div class="container-fluid">

            <div>
                <p class="caption grey-clr-txt">Reviews</p>
                <div class="caption_line grey-clr-bg"></div>

                <div class="text-center">
                    <img src="{{ asset('image/quotes.png') }}">
                </div>
            </div>

            <div id="reviews_slide" class="carousel slide" data-ride="carousel" data-interval="false">
                <!-- Left and right controls -->
                <a class="carousel-control-prev" href="#reviews_slide" data-slide="prev">
                    <img src="{{ asset('image/arrow_left.png') }}">
                </a>
                <a class="carousel-control-next" href="#reviews_slide" data-slide="next">
                    <img src="{{ asset('image/arrow_right.png') }}">
                </a>

                <!-- The slideshow -->
                <div class="carousel-inner col-9 m-auto">
                    @foreach($data['reviews'] as $key => $review)
                        <div class="carousel-item {{$key == 0 ? 'active' : '' }}"">
                        <p class="review_main wow fadeIn" data-wow-duration="1s">
                            {!! $review->review !!}
                        </p>
                        <div class="m-auto text-center">
                            <img src="{{ asset('image/Vadim_Tumarkin.png') }}"
                                 class="col-md-5 review_author_photo wow zoomIn"
                                 data-wow-delay="0.2s" data-wow-duration="0.8s">
                            <p class="review_author sf-700-font wow slideInLeft" data-wow-delay="0.6s">
                                {{ $review->designator }}
                            </p>
                            <p class="review_text sf-500-font grey-clr-txt
                            wow slideInRight" data-wow-delay="0.6s">
                                {!! $review->review !!}
                            </p>
                        </div>
                </div>
                @endforeach
            </div>

            <!-- Indicators -->
            <ul class="carousel-indicators position-static">
                @foreach($data['reviews'] as $key => $review)
                    <li data-target="#reviews_slide" data-slide-to="{{ $key }}" class="{{$key == 0 ? 'active' : '' }}"></li>
                @endforeach
            </ul>
        </div>
        </div>

    </section>
    <section class="main contact white-grey-clr-bg">
        <div class="container-fluid">
            <p class="caption grey-clr-txt">Get in Touch</p>
            <div class="caption_line grey-clr-bg"></div>
            <div class="row">
                <div class="col-md-5 m-auto">
                    <p class="text grey-clr-txt sf-400-font wow slideInLeft">Email us with any questions or inquiries or
                        use our contact
                        data.</p>
                    <form class="wow fadeIn" data-wow-duration="3s" method="post" action="{{ route('send-mail') }}">
                        @csrf
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <input name="name" class="form-control border-0 rounded-0 sf-400-font" id="inputName"
                                       placeholder="Name">
                            </div>
                            <div class="form-group col-md-6">
                                <input type="email" name="email" class="form-control border-0 rounded-0 sf-400-font" id="inputEmail"
                                       placeholder="Email">
                            </div>
                        </div>
                        <div class="form-group">
                            <textarea class="form-control border-0 rounded-0 sf-400-font" placeholder="Message"
                                      style="height: 200px;" name="body"></textarea>
                        </div>
                        <button type="submit" class="contact-us_btn btn-sm btn btn-grey btn-block sf-700-font">
                            CONTACT US NOW
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
