@extends('layout.panda-main')

@section('title')
    News
@endsection

@section('content')
    <section class="news_list_content">
        <div class="container">
            <div class="col-md-10 ml-md-auto mr-md-auto">
                <div class="d-flex flex-wrap justify-content-between">
                    @foreach($newsList as $news)
                        <div class="card border-0 col-md-3 col-lg-4">
                            <img class="news_list_img" src="{{ asset('image/news_image.png') }}" alt="">
                            <div class="card-body pl-0">
                                <a href="{{ route('news-show', $news->id) }}">
                                    <h6 class="card-title sf-500-font grey-clr-txt">
                                        {{ $news->title }}</h6>
                                </a>
                                <div class="card-text sf-500-font light-grey-clr-txt">
                                    <img class="calendar-icon" src="{{ asset('image/calendar_icon.png') }}">
                                    <span class="grey-color">
                                            {{ $news->created_at->format('M d, Y') }}
                                        </span>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
                {{ $newsList->links('include.pagination') }}
            </div>
        </div>
    </section>
@endsection


@section('header')
    @extends('include.header', [
        'locations' => [
            [
                'link' => route('news-list'),
                'name' => 'News'
            ]
        ]
    ])
@endsection
