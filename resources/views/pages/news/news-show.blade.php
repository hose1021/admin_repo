@extends('layout.panda-main')

@section('title')
    News
@stop

@section('content')
    <section class="news_content">
        <div class="container">
            <div class="col-md-10 mr-md-auto ml-md-auto">
                <div class="sf-500-font light-grey-clr-txt">
                    <img class="calendar-icon" src="{{ asset('image/calendar_icon.png') }}"> <span class="grey-color">
                                {{ $news->created_at->format('M d, Y') }}
                            </span>
                </div>
                <h5 class="news_title">
                    {{ $news->title }}
                </h5>
                {!! $news->body !!}
            </div>
        </div>
    </section>
@stop


@section('header')
    @include('include.header', [
        'locations' => [
            [
                'link' => route('news-list'),
                'name' => 'News'
            ],
            [
                'link' => route('news-show', $news->id),
                'name' => $news->title
            ]
        ]
    ])
@stop
