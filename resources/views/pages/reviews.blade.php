@extends('layout.panda-main')

@section('title')
    Reviews
@endsection

@section('content')
    <section class="reviews_content">
        <div class="container">
            <div class="content">
                @foreach($reviews as $review)
                    <div class="col-md-11 m-auto">
                        <div class="row">
                            <div class="col-11 p-0">
                                <p class="review_text dark-grey-clr-txt">
                                    {{ $review->review }}
                                </p>
                            </div>
                            <div class="col-1 review_quotes">
                                <img src="assets/image/quotes_icon.png" alt="">
                            </div>
                        </div>
                        <div class="row">
                            <div class="review_author_photo">
                                <img src="assets/image/Vadim_Tumarkin.png">
                            </div>
                            <div class="col-10">
                                <p class="sf-500-font review_author m-0">{{ $review->designator }}</p>
                                <p class="review_text grey-clr-txt">{{ $review->position }}</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-10 review_separator"></div>
                @endforeach
            </div>
            {{ $reviews->links('include.pagination') }}
        </div>
    </section>
@endsection


@section('header')
    @extends('include.header', [
        'locations' => [
            [
                'link' => route('reviews'),
                'name' => 'Reviews'
            ]
        ]
    ])
@endsection
