@extends('layout.panda-main')

@section('title')
    Contact
@endsection

@section('content')
    <section class="contact_content m-b-40">
        <div class="container">
            <div class="row">
                <div class="offset-md-1 col-md-3 contacts wow slideInLeft">
                    <ul class="list-group">
                        <li class="list-group-item bg-transparent border-0 p-0 grey-clr-txt">
                            <h5>Address:</h5>
                            <div class="row">
                                <div class="col-1">
                                    <i class="fas fa-map-marker-alt p-0"></i>
                                </div>
                                <div class="col-10">
                                    <p class="m-0">59, Rashid Behbudov str.,</p>
                                    <p>Baku, Azerbaijan, AZ1022</p>
                                </div>
                            </div>
                        </li>
                        <li class="list-group-item bg-transparent border-0 p-0  grey-clr-txt">
                            <h5>Phone:</h5>
                            <div class="row">
                                <div class="col-1">
                                    <i class="fas fa-phone-alt"></i>
                                </div>
                                <div class="col-9">
                                    <a href="skype:+994124973737?call">
                                        <p>+994 12 497 3737</p>
                                    </a>
                                </div>
                            </div>
                        </li>
                        <li class="list-group-item bg-transparent border-0 p-0  grey-clr-txt">
                            <h5>Fax:</h5>
                            <div class="row">
                                <div class="col-1">
                                    <img src="assets/image/printer_icon_dark.png" style="width: 16px">
                                </div>
                                <div class="col-9">
                                    <p>+994 12 498 1993</p>
                                </div>
                            </div>
                        </li>
                        <li class="list-group-item bg-transparent border-0 p-0 grey-clr-txt">
                            <h5>Email:</h5>
                            <div class="row">
                                <div class="col-1">
                                    <i class="fas fa-envelope"></i>
                                </div>
                                <div class="col-9">
                                    <a href="mailto:pandasales@risk.az">
                                        pandasales@risk.az
                                    </a>
                                    <a href="mailto:pandasupport@risk.az">
                                        pandasupport@risk.az
                                    </a>
                                    <a href="mailto:pandasupport@risk.az">
                                        pandasupport@risk.az
                                    </a>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="col-md-7 information wow slideInRight">
                    <h4 class="grey-clr-txt sf-700-font">Miscellaneous information:</h4>
                    <p class="grey-clr-txt sf-500-font">Email us with any questions or
                        inquiries or use our contact data.</p>
                    <form method="post" action="{{ route('send-mail') }}">
                        @csrf
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <input name="name" class="form-control border-0 rounded-0 white-grey-clr-bg"
                                       id="inputName" placeholder="Name">
                            </div>
                            <div class="form-group col-md-6">
                                <input name="email" type="email" class="form-control border-0 rounded-0 white-grey-clr-bg"
                                       id="inputEmail" placeholder="Email">
                            </div>
                        </div>
                        <div class="form-group">
                                    <textarea class="form-control border-0 rounded-0 white-grey-clr-bg"
                                              placeholder="Message" name="body" style="height: 160px;"></textarea>
                        </div>
                        <button class="col-md-12 btn btn-grey" type="submit">
                            CONTACT US NOW</button>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection


@section('header')
    @extends('include.header', [
        'locations' => [
            [
                'link' => route('contact'),
                'name' => 'Contact'
            ]
        ]
    ])
@endsection
