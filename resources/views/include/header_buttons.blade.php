<header class="main_header">
    <div class="d-flex justify-content-between">
        <div class="main_logo">
            <a href="/" class="navbar-brand">
                <img src="{{ asset('image/main_logo.png') }}"/>
            </a>
        </div>

        <nav class="navbar navbar-expand-md navbar-dark">
            <div class="menu_panel">
                <ul class="navbar-nav mr-auto justify-content-end">
                    <li class="nav-item">
                        <a class="nav-link text-white sf-500-font" href="#">
                            English
                        </a>
                    </li>
                    <ul class="social-frame navbar-nav justify-content-end" style="position: relative">
                        <li class="nav-item social-link">
                            <a class="visit nav-link text-white sf-500-font" href="#">
                                Visit our site
                            </a>
                        </li>
                        <ul class="social-icons-frame navbar-nav justify-content-end">
                            <li class="nav-item social-icons1">
                                <a class="visit nav-link text-white sf-500-font" href="#">
                                    <img src="{{ asset('image/icon/facebook_icon.png') }}" alt="" style="width:23px">
                                </a>
                            </li>
                            <li class="nav-item social-icons2">
                                <a class="visit nav-link text-white sf-500-font" href="#">
                                    <img src="{{ asset('image/icon/facebook_icon.png') }}" alt="" style="width:23px">
                                </a>
                            </li>
                            <li class="nav-item social-icons3">
                                <a class="visit nav-link text-white sf-500-font" href="#">
                                    <img src="{{ asset('image/icon/facebook_icon.png') }}" alt="" style="width:23px">
                                </a>
                            </li>
                        </ul>
                    </ul>

                </ul>
                @include('include.menu')
            </div>
        </nav>
    </div>
    <div class="main_caption container">
        <p>Advanced solutions</p>
        <p>for safe and efficient air navigation</p>
        <div class="row">
            <div class="col-md-5 m-auto">
                <a class="btn btn-white">
                    EXPLORE VISUALS
                </a>
                <a class="btn btn-white-border">
                    VIEW PRODUCTS
                </a>
            </div>
        </div>
    </div>

</header>
