<header>
    <div class="page-header">
        <nav class="navbar navbar-expand-md sticky-top navbar-dark">
            <div class="container-fluid">
                <a href="/" class="navbar-brand main_logo">
                    <img src="{{ asset('image/main_logo.png') }}" />
                </a>
                <div>
                    @include('include.menu')
                </div>
            </div>
        </nav>
    </div>
    <div class="navigation grey-clr-bg">
        <ul>
            <li>
                <a href="/" class="light-grey-clr-txt">Home</a>
            </li>
            @foreach((array) $locations as $location)
                <li>
                    <img src="{{ asset('image/navigation.png') }}" alt="">
                </li>
                <li>
                    <a href="{{ $location['link'] }}" class="light-grey-clr-txt">{{ $location['name'] }}</a>
                </li>
            @endforeach
        </ul>
    </div>
</header>
