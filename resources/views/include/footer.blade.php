<div class="container-fluid main-footer">
    <div class="up-btn">
        <img src="{{ asset('image/up-button.png') }}" alt="">
    </div>
    <div class="d-flex flex-wrap justify-content-around">
        <div class="company col-md-6">
            <img class="main-logo" src="{{ asset('image/main_logo.png') }}" />
            <p class="text-white text-justify sf-400-font">R.I.S.K. Company regularly extends the package of
                aeronautical applications thus contributing to the
                "gate to gate" concept of modern aviation.
                Our Aeronautical Software Systems are being used by
                flight procedure and air space designers, ATC Services,
                airport services, AIS and Divisions of Aeronautical Cartographers. </p>
            <img src="{{ asset('image/footer-logo.png') }}" style="width:85px">
        </div>
        <div class="navigation col-md-1 p-0">
            <h5 class="sf-500-font">Navigation</h5>
            @include('include.menu')
        </div>
        <div class="col-md-3 contact">
            <h5 class="sf-500-font">Contact</h5>
            <ul class="list-group">
                <li class="list-group-item bg-transparent border-0 p-0">
                    <div class="row">
                        <div class="col-2">
                            <i class="fas fa-map-marker-alt p-0"></i>
                        </div>
                        <div class="col-10">
                            <p>59, Rashid Behbudov str. Baku, Azerbaijan, AZ1022</p>
                        </div>
                    </div>
                </li>
                <li class="list-group-item bg-transparent border-0 p-0">
                    <div class="row">
                        <div class="col-2">
                            <i class="fas fa-phone-alt"></i>
                        </div>
                        <div class="col-10">
                            <a href="skype:+994124973737?call">
                                <p>+994 12 497 3737</p>
                            </a>
                        </div>
                    </div>
                </li>
                <li class="list-group-item bg-transparent border-0 p-0">
                    <div class="row">
                        <div class="col-2">
                            <img src="{{ asset('image/printer_icon.png') }}">
                        </div>
                        <div class="col-10">
                            <p>+994 12 498 1993</p>
                        </div>
                    </div>
                </li>
                <li class="list-group-item bg-transparent border-0 p-0">
                    <div class="row">
                        <div class="col-2">
                            <i class="fas fa-envelope"></i>
                        </div>
                        <div class="col-10">
                            <p>
                                <a href="mailto:pandasales@risk.az">
                                    pandasales@risk.az
                                </a>
                                <br>
                                <a href="mailto:pandasupport@risk.az">
                                    pandasupport@risk.az
                                </a>
                                <br>
                                <a href="mailto:pandasupport@risk.az">
                                    pandasupport@risk.az
                                </a>
                            </p>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
    <div class="copyright m-t-20">
        <span>&copy 2019</span>
        <span style="padding-left: 35px">Pandanavigation. All rights reserved.</span>
    </div>
</div>
