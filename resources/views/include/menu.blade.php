<ul class="navbar-nav mr-auto">
    <li class="nav-item">
        <a href="{{ route('home') }}" class="nav-link text-white sf-500-font">Home</a>
    </li>
    <li class="nav-item">
        <a href="products.html" class="nav-link text-white sf-500-font">Products</a>
    </li>
    <li class="nav-item">
        <a href="#" class="nav-link text-white sf-500-font">Customers</a>
    </li>
    <li class="nav-item">
        <a href="{{ route('reviews') }}" class="nav-link text-white sf-500-font">Reviews</a>
    </li>
    <li class="nav-item">
        <a href="{{ route('news-list') }}" class="nav-link text-white sf-500-font">News</a>
    </li>
    <li class="nav-item">
        <a href="{{ route('contact') }}" class="nav-link text-white sf-500-font">Contact</a>
    </li>
    <li class="nav-item">
        <a href="#" class="nav-link text-white sf-500-font">Forum</a>
    </li>
</ul>
