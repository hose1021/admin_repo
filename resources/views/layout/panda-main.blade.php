<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="UTF-8">
    <title>@yield('title', '') | Panda</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('css/fonts.css') }}">
    <link rel="stylesheet" href="{{ asset('css/main.css') }}">
    <link rel="stylesheet" href="{{ asset('css/animate.css') }}">
    <link rel="stylesheet" href="{{ asset('slick/slick.css') }}">
    <link rel="stylesheet" href="{{ asset('slick/slick-theme.css') }}">
    <script src="https://kit.fontawesome.com/cec2014a6c.js"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
            integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
            integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
            integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
    </script>
</head>
<body>
@yield('header')
@yield('content')
<footer>
    @include('include.footer')
</footer>
<script src="{{ asset('js/jquery-3.4.1.min.js') }}"></script>
<script src="{{ asset('slick/slick.js') }}"></script>
<script src="{{ asset('js/wow.js') }}"></script>
<script>
    new WOW().init();
</script>
<script>
    // NEWS SLIDER
    $(document).ready(function () {
        $('.demo').slick({
            dots: true,
            arrows: false,
            slidesToShow: 3,
            slidesToScroll: 3,
            touchMove: false
        });
    });
</script>
<script>
    // UP BTN
    jQuery(document).ready(function ($) {
        var visible = false;
        $(window).scroll(function () {
            var scrollTop = $(this).scrollTop();
            if (!visible && scrollTop > 100) {
                $(".up-btn").fadeIn();
                visible = true;
            } else if (visible && scrollTop <= 100) {
                $(".up-btn").fadeOut();
                visible = false;
            }
        });
        $(".up-btn").click(function () {
            $("html, body").animate({
                scrollTop: 0
            }, "fast");
            return false;
        });
    });
</script>

</body>

</html>
