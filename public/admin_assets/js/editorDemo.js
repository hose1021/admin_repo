(function ($) {
    'use strict';

    /*Summernote editor*/
    if ($("#summernote_az, #summernote_en, #summernote_ru, #summernote").length) {
        $('#summernote_az, #summernote_en, #summernote_ru, #summernote').summernote({
            height: 300,
            tabsize: 2
        });
    }

    if ($("#summernote_address, #summernote_phone, #summernote_fax, #summernote_emails").length) {
        $('#summernote_address, #summernote_phone, #summernote_fax, #summernote_emails').summernote({
            height: 300,
            tabsize: 2
        });
    }

    if ($('#description').length) {
        $('#description').summernote({
            height: 300,
            tabsize: 2
        });
    }
})(jQuery);
