<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLocalizedTextsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('localized_texts', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->text('tbl_name', 65535)->nullable();
			$table->text('tbl_column_name', 65535)->nullable();
			$table->integer('idx')->nullable();
			$table->text('ru', 65535)->nullable();
			$table->text('az', 65535)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('localized_texts');
	}

}
