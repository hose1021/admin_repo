<?php

Auth::routes();

Route::group(['prefix' => '/'], function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('news', 'NewsController@index')->name('news-list');
    Route::get('news/{id}', 'NewsController@show')->name('news-show');
    Route::get('contact', 'ContactController@index')->name('contact');
    Route::get('reviews', 'ReviewController@index')->name('reviews');
    Route::post('mail', 'ContactController@sendEmail')->name('send-mail');
});

Route::group(['prefix' => 'admin', 'middleware' => 'auth',], function () {

    Route::get('/', 'Admin\HomeController@index')->name('admin.index');

    Route::resource('product', 'Admin\ProductController')->except([
        'show'
    ]);

    Route::resource('news', 'Admin\NewsController')->except([
        'show'
    ]);

    Route::resource('review', 'Admin\ReviewController')->except([
        'show'
    ]);

    Route::resource('contacts-email', 'Admin\ContactsEmailController')->except([
        'show'
    ]);

    Route::resource('contacts', 'Admin\ContactsController')->except([
        'show'
    ]);

    Route::resource('customers', 'Admin\CustomerController')->except([
        'show'
    ]);

    Route::post('image-upload/{id}', 'Admin\ProductImagesController@upload');

    Route::post('image-destroy/{id}', 'Admin\ProductImagesController@destroy');

    Route::post('image-store', 'Admin\ProductImagesController@store');
});
