-- --------------------------------------------------------
-- Хост:                         127.0.0.1
-- Версия сервера:               10.3.16-MariaDB - mariadb.org binary distribution
-- Операционная система:         Win64
-- HeidiSQL Версия:              10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Дамп структуры базы данных admin
CREATE DATABASE IF NOT EXISTS `admin` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `admin`;

-- Дамп структуры для таблица admin.localized_texts
CREATE TABLE IF NOT EXISTS `localized_texts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tbl_name` text DEFAULT NULL,
  `tbl_column_name` text DEFAULT NULL,
  `idx` int(11) DEFAULT NULL,
  `ru` text DEFAULT NULL,
  `az` text DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы admin.localized_texts: ~11 rows (приблизительно)
/*!40000 ALTER TABLE `localized_texts` DISABLE KEYS */;
INSERT INTO `localized_texts` (`id`, `tbl_name`, `tbl_column_name`, `idx`, `ru`, `az`) VALUES
	(15, 'posts', 'body', 11, 'asdasdasdsad', '<p>asdasdasd</p>'),
	(16, 'posts', 'short_desc', 11, 'asd', 'a'),
	(17, 'posts', 'title', 11, 'asd', 'agdhsgadhgasd'),
	(18, 'posts', 'body', 13, NULL, NULL),
	(19, 'posts', 'short_desc', 13, NULL, NULL),
	(20, 'posts', 'title', 13, NULL, NULL),
	(21, 'posts', 'body', 14, NULL, NULL),
	(22, 'posts', 'short_desc', 14, NULL, NULL),
	(23, 'posts', 'title', 14, NULL, NULL),
	(24, 'posts', 'body', 15, NULL, NULL),
	(25, 'posts', 'short_desc', 15, NULL, NULL),
	(26, 'posts', 'title', 15, NULL, NULL);
/*!40000 ALTER TABLE `localized_texts` ENABLE KEYS */;

-- Дамп структуры для таблица admin.migrations
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Дамп данных таблицы admin.migrations: ~4 rows (приблизительно)
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(1, '2014_10_12_000000_create_users_table', 1),
	(2, '2014_10_12_100000_create_password_resets_table', 1),
	(4, '2019_08_30_094927_reviews', 2),
	(5, '2019_08_30_094625_products', 3);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Дамп структуры для таблица admin.news
CREATE TABLE IF NOT EXISTS `news` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(191) DEFAULT NULL,
  `short_desc` char(50) DEFAULT NULL,
  `body` longtext DEFAULT NULL,
  `author_id` int(11) NOT NULL,
  `main_active` char(50) DEFAULT NULL,
  `status` int(10) unsigned NOT NULL DEFAULT 1,
  `published_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы admin.news: ~4 rows (приблизительно)
/*!40000 ALTER TABLE `news` DISABLE KEYS */;
INSERT INTO `news` (`id`, `title`, `short_desc`, `body`, `author_id`, `main_active`, `status`, `published_at`, `created_at`, `updated_at`) VALUES
	(11, 'asdasd', NULL, '<p>asdad</p>', 1, NULL, 0, '2019-08-28 14:26:36', '2019-08-28 10:26:36', '2019-08-29 14:40:23'),
	(12, NULL, NULL, NULL, 1, NULL, 1, '2019-08-29 23:00:22', '2019-08-29 19:00:22', '2019-08-29 19:00:22'),
	(13, NULL, NULL, NULL, 1, NULL, 1, '2019-08-29 23:01:39', '2019-08-29 19:01:39', '2019-08-29 19:01:39'),
	(14, 'фыв', 'фыв', '<p>фывфыв</p>', 1, NULL, 1, '2019-08-29 23:02:13', '2019-08-29 19:02:13', '2019-08-29 19:02:13'),
	(15, 'фывфыв', 'фывыв', '<p>фывфывфыв</p>', 1, 'on', 1, '2019-08-29 23:04:26', '2019-08-29 19:04:26', '2019-08-29 19:04:26');
/*!40000 ALTER TABLE `news` ENABLE KEYS */;

-- Дамп структуры для таблица admin.password_resets
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Дамп данных таблицы admin.password_resets: ~0 rows (приблизительно)
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;

-- Дамп структуры для таблица admin.products
CREATE TABLE IF NOT EXISTS `products` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `images` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `short_desc` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Дамп данных таблицы admin.products: ~0 rows (приблизительно)
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
INSERT INTO `products` (`id`, `name`, `images`, `short_desc`, `body`, `status`, `created_at`, `updated_at`) VALUES
	(1, '123', '', 'asdasd', '', '1', '2019-08-25 15:21:47', '2019-08-30 14:11:29');
/*!40000 ALTER TABLE `products` ENABLE KEYS */;

-- Дамп структуры для таблица admin.reviews
CREATE TABLE IF NOT EXISTS `reviews` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `designator` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `position` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `review` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `data_time` date NOT NULL,
  `status` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Дамп данных таблицы admin.reviews: ~0 rows (приблизительно)
/*!40000 ALTER TABLE `reviews` DISABLE KEYS */;
/*!40000 ALTER TABLE `reviews` ENABLE KEYS */;

-- Дамп структуры для таблица admin.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Дамп данных таблицы admin.users: ~0 rows (приблизительно)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
	(1, 'hose1021', 'hose1021@gmail.com', NULL, '$2y$10$Cu8arbIjNF0B1ahzcSQrZ.2CE/OCoda./UEAw1SFNPZXp92/ke5Ae', NULL, '2019-08-25 15:21:47', '2019-08-25 15:21:47');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
