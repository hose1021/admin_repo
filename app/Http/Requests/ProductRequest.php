<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title_az'      => 'string|nullable',
            'short_desc_az' => 'string|max:50|nullable',
            'body_az'       => 'string|nullable',
            'title_ru'      => 'string|nullable',
            'short_desc_ru' => 'string|max:50|nullable',
            'body_ru'       => 'string|nullable',
            'title_en'      => 'string|nullable',
            'short_desc_en' => 'string|max:50|nullable',
            'body_en'       => 'string|nullable',
            'main_image'    => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'image.*'       => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ];
    }
}
