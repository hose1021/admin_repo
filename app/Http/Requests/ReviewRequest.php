<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ReviewRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'designator_az' => 'string|nullable',
            'position_az'   => 'string|max:50|nullable',
            'review_az'     => 'string|nullable',
            'designator_ru' => 'string|nullable',
            'position_ru'   => 'string|max:50|nullable',
            'review_ru'     => 'string|nullable',
            'designator_en' => 'string|nullable',
            'position_en'   => 'string|max:50|nullable',
            'review_en'     => 'string|nullable',
            'datetime'      => 'date_format:Y-m-d H:i',
        ];
    }
}
