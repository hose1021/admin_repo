<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ContactsEmailRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name_az'    => 'string|nullable',
            'email_az'   => 'string|max:50|nullable',
            'message_az' => 'string|nullable',
            'name_ru'    => 'string|nullable',
            'email_ru'   => 'string|max:50|nullable',
            'message_ru' => 'string|nullable',
            'name_en'    => 'string|nullable',
            'email_en'   => 'string|max:50|nullable',
            'message_en' => 'string|nullable',
            'datetime'   => 'date_format:Y-d-m H:i',
        ];
    }
}
