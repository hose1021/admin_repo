<?php declare(strict_types=1);
namespace App\Http\Traits;

trait LocalizedService
{

    public function translation($lang, $tabindex, $index)
    {
        switch ($lang) {
            case "az":
                $translated = $this->localizedText->where('tbl_column_name', $tabindex[$index])->first();
                if ($translated != NULL)
                    return $translated->az;
                else break;
            case "ru":
                $translation = $this->localizedText->where('tbl_column_name', $tabindex[$index])->first();
                if ($translation != NULL)
                    return $translation->ru;
                else break;
        }
    }
}
