<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\CustomerRequest;
use App\Models\Customer;
use App\Models\Product;
use Illuminate\Http\Response;
use Illuminate\View\View;

class CustomerController extends Controller
{
    /**
     * CustomerController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth', ['only' => ['index', 'edit', 'update', 'store', 'destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return View
     */
    public function index(): View
    {
        $customers = Customer::paginate(10);
        return view('admin.customers.list', compact('customers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return View
     */
    public function create(): View
    {
        $products = Product::get();
        return view('admin.customers.create', compact('products'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CustomerRequest $request
     * @return View
     */
    public function store(CustomerRequest $request): View
    {
        $customer = new Customer([
            'name'        => $request->get('name'),
            'latitude'    => $request->get('latitude'),
            'longitude'   => $request->get('longitude'),
            'description' => $request->get('body'),
        ]);
        $products = $request->input('products');
        $customer->product()->sync($products);
        $customer->save();

        return back()->with('success', 'Stock has been added');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function edit(int $id): View
    {
        $customers = Customer::FindorFail($id)->firstOrFail();
        $products = Product::get();
        return view('admin.customers.edit', [
            'customers' => $customers,
            'products'  => $products
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param CustomerRequest $request
     * @param int $id
     * @return View
     */
    public function update(CustomerRequest $request, int $id): View
    {
        $customer = Customer::findOrFail($id);
        $customer->name = $request->get('name');
        $customer->latitude = $request->get('latitude');
        $customer->longitude = $request->get('longitude');
        $products = $request->input('products');
        $customer->product()->sync($products);
        $customer->save();

        return back()->with('success', 'Customer has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return View
     */
    public function destroy(int $id): View
    {
        $customer = Customer::findorfail($id);
        if ($customer->status == 0) {
            $customer->status = '1';
        } elseif ($customer->status == 1) {
            $customer->status = '0';
        }
        $customer->save();
        return back();
    }
}
