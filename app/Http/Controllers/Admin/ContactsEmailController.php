<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\ContactsEmailRequest;
use App\Models\Email;
use App\Models\Localized;
use Illuminate\Http\Response;

class ContactsEmailController extends Controller
{
    /**
     * ContactsEmailController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth', ['only' => ['index', 'edit', 'update', 'store', 'destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $emails = Email::paginate(10);
        return view('admin.emails.list', compact('emails'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.emails.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ContactsEmailRequest $request
     * @return Response
     */
    public function store(ContactsEmailRequest $request)
    {

        $email = new Email([
            'name'     => $request->get('name_en'),
            'email'    => $request->get('email_en'),
            'message'  => $request->get('message_en'),
            'datetime' => $request->get('datetime'),
        ]);
        $email->save();

        $email_name = new Localized([
            'tbl_name'        => 'contacts_email',
            'tbl_column_name' => 'name',
            'idx'             => $email->id,
            'ru'              => $request->get('name_ru'),
            'az'              => $request->get('name_az'),
        ]);

        $email_email = new Localized([
            'tbl_name'        => 'contacts_email',
            'tbl_column_name' => 'email',
            'idx'             => $email->id,
            'ru'              => $request->get('email_ru'),
            'az'              => $request->get('email_az'),
        ]);

        $email_message = new Localized([
            'tbl_name'        => 'contacts_email',
            'tbl_column_name' => 'message',
            'idx'             => $email->id,
            'ru'              => $request->get('message_ru'),
            'az'              => $request->get('message_az'),
        ]);
        $email_message->save();
        $email_email->save();
        $email_name->save();

        return back()->with('success', 'Contacts email has been added');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function edit(int $id)
    {
        $emails = Email::FindorFail($id)->with('localizedText')->where('id', $id)->firstOrFail();
        return view('admin.emails.edit', compact('emails'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param ContactsEmailRequest $request
     * @param int $id
     * @return Response
     */
    public function update(ContactsEmailRequest $request, int $id)
    {
        $email = Email::findOrFail($id)->with('localizedText')->where('id', $id)->firstOrFail();
        $email->name = $request->get('name_en');
        $email->email = $request->get('email_en');
        $email->message = $request->get('message_en');
        $email->datetime = $request->get('datetime');
        $email->save();

        Localized::updateOrInsert(['idx' => $email->id, 'tbl_column_name' => 'name'], [
            'tbl_name'        => 'contacts_email',
            'tbl_column_name' => 'name',
            'az'              => $request->get('name_az'),
            'ru'              => $request->get('name_ru'),
        ]);

        Localized::updateOrInsert(['idx' => $email->id, 'tbl_column_name' => 'email'], [
            'tbl_name'        => 'contacts_email',
            'tbl_column_name' => 'email',
            'az'              => $request->get('email_az'),
            'ru'              => $request->get('email_ru'),
        ]);

        Localized::updateOrInsert(['idx' => $email->id, 'tbl_column_name' => 'message'], [
            'tbl_name'        => 'contacts_email',
            'tbl_column_name' => 'message',
            'az'              => $request->get('message_az'),
            'ru'              => $request->get('message_ru'),
        ]);

        return back()->with('success', 'Contacts email has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function destroy(int $id)
    {
        $email = Email::findorfail($id);
        if ($email->status == 0) {
            $email->status = '1';
        } elseif ($email->status == 1) {
            $email->status = '0';
        }
        $email->save();
        return back();
    }
}
