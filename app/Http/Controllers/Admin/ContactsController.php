<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Contacts;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ContactsController extends Controller
{
    /**
     * ContactsController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth', ['only' => ['index', 'edit', 'update', 'store', 'destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $contacts = Contacts::paginate(10);
        return view('admin.contacts.list', compact('contacts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.contacts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'address' => 'string|nullable',
            'phone'   => 'string|nullable',
            'fax'     => 'string|nullable',
            'emails'  => 'string|nullable',
        ]);

        $contacts = new Contacts([
            'address' => $request->get('address'),
            'phone'   => $request->get('phone'),
            'fax'     => $request->get('fax'),
            'emails'  => $request->get('emails'),
        ]);
        $contacts->save();

        return back()->with('success', 'Contacts has been added');
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Admin\Contacts $contacts
     * @return Response
     */
    public function show(Contacts $contacts)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function edit(int $id)
    {
        $contacts = Contacts::FindorFail($id);
        return view('admin.contacts.edit', compact('contacts'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return void
     */
    public function update(Request $request, int $id)
    {
        $request->validate([
            'address' => 'string|nullable',
            'phone'   => 'string|nullable',
            'fax'     => 'string|nullable',
            'emails'  => 'string|nullable',
        ]);

        $contact = Contacts::findOrFail($id)->firstOrFail();
        $contact->address = $request->get('address');
        $contact->phone = $request->get('phone');
        $contact->fax = $request->get('fax');
        $contact->emails = $request->get('emails');
        $contact->save();

        return back()->with('success', 'Contacts has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function destroy(int $id)
    {
        $contact = Contacts::findorfail($id);
        if ($contact->status == 0) {
            $contact->status = '1';
        } elseif ($contact->status == 1) {
            $contact->status = '0';
        }
        $contact->save();
        return back();
    }
}
