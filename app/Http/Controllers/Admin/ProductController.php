<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProductRequest;
use App\Models\Localized;
use App\Models\Media;
use App\Models\Product;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Response;
use Illuminate\View\View;
use Illuminate\Support\Facades\Auth;

class ProductController extends Controller
{
    /**
     * ProductController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth', ['only' => ['index', 'edit', 'update', 'store', 'destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $products = Product::paginate(10);
        return view('admin.products.list', compact('products'));
    }

    /**
     * @return Factory|View
     *
     */
    public function create()
    {
        return view('admin.products.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ProductRequest $request
     * @return string
     */
    public function store(ProductRequest $request)
    {
        $product = new Product([
            'name'       => $request->get('title_en'),
            'short_desc' => $request->get('short_desc_en'),
            'body'       => $request->get('body_en'),
            'author_id'  => Auth::user()->id,
        ]);
        $product->save();

        if ($image = $request->file('image')) {
            foreach ($image as $files) {
                $destinationPath = 'media/product/'; // upload path
                if (!$files->move($destinationPath, $files->getClientOriginalName())) {
                    return 'Error saving the file.';
                }
                $form = new Media([
                    'model_type'      => 'Products',
                    'model_id'        => $product->id,
                    'collection_name' => 'product_image',
                    'file_name'       => $files->getClientOriginalName(),

                ]);
                $form->save();
            }
        }

        if ($image = $request->file('main_image')) {
            $destinationPath = 'media/product';
            if (!$image->move($destinationPath, $image->getClientOriginalName())) {
                return 'Error saving the file.';
            }
            $form = new Media([
                'model_type'      => 'Products',
                'model_id'        => $product->id,
                'collection_name' => 'product_main_image',
                'file_name'       => $image->getClientOriginalName(),
            ]);
            $form->save();
        }

        $product_local_title = new Localized([
            'tbl_name'        => 'products',
            'tbl_column_name' => 'title',
            'idx'             => $product->id,
            'ru'              => $request->get('title_ru'),
            'az'              => $request->get('title_az'),
        ]);

        $product_local_short_desc = new Localized([
            'tbl_name'        => 'products',
            'tbl_column_name' => 'short_desc',
            'idx'             => $product->id,
            'ru'              => $request->get('short_desc_ru'),
            'az'              => $request->get('short_desc_az'),
        ]);

        $product_local_body = new Localized([
            'tbl_name'        => 'products',
            'tbl_column_name' => 'body',
            'idx'             => $product->id,
            'ru'              => $request->get('body_ru'),
            'az'              => $request->get('body_az'),
        ]);
        $product_local_body->save();
        $product_local_short_desc->save();
        $product_local_title->save();

        return back()->with('success', 'ProductRequest has been added');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return void
     */
    public function edit(int $id)
    {
        $product = Product::FindorFail($id)->with('localizedText')->with('productImage')->where('id',
            $id)->firstOrFail();
        return view('admin.products.edit', compact('product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param ProductRequest $request
     * @param int $id
     * @return string
     */
    public function update(ProductRequest $request, int $id)
    {
        $product = Product::findOrFail($id)->with('localizedText')->where('id', $id)->firstOrFail();
        $product->name = $request->get('title_en');
        $product->short_desc = $request->get('short_desc_en');
        $product->body = $request->get('body_en');
        if ($image = $request->file('main_image')) {
            $destinationPath = 'media/product';
            if (!$image->move($destinationPath, $image->getClientOriginalName())) {
                return 'Error saving the file.';
            }
            $form = new Media([
                'model_type'      => 'Products',
                'model_id'        => $product->id,
                'collection_name' => 'product_main_image',
                'file_name'       => $image->getClientOriginalName(),
            ]);
            $form->save();
        }
        $product->save();

        Localized::updateOrInsert(['idx' => $product->id, 'tbl_column_name' => 'title'], [
            'tbl_name'        => 'products',
            'tbl_column_name' => 'title',
            'az'              => $request->get('title_az'),
            'ru'              => $request->get('title_ru'),
        ]);

        Localized::updateOrInsert(['idx' => $product->id, 'tbl_column_name' => 'body'], [
            'tbl_name'        => 'products',
            'tbl_column_name' => 'body',
            'az'              => $request->get('body_az'),
            'ru'              => $request->get('body_ru'),
        ]);

        Localized::updateOrInsert(['idx' => $product->id, 'tbl_column_name' => 'short_desc'], [
            'tbl_name'        => 'products',
            'tbl_column_name' => 'short_desc',
            'az'              => $request->get('short_desc_az'),
            'ru'              => $request->get('short_desc_ru'),
        ]);

        return back()->with('success', 'ProductRequest has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function destroy(int $id)
    {
        $product = Product::findorfail($id);
        if ($product->status == 0) {
            $product->status = '1';
        } elseif ($product->status == 1) {
            $product->status = '0';
        }
        $product->save();
        return back();
    }
}
