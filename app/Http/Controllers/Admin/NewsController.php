<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\NewsRequest;
use App\Models\Media;
use App\Models\Localized;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use App\Models\News;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class NewsController extends Controller
{
    /**
     * NewsController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth', ['only' => ['index', 'edit', 'update', 'store', 'destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return View
     */
    public function index(): View
    {
        $news = News::paginate(10);
        return view('admin.news.list', compact('news'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return View
     */
    public function edit(int $id): View
    {
        $news = News::FindorFail($id)->with('localizedText')->where('id', $id)->firstOrFail();
        return view('admin.news.edit', compact('news'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return View
     */
    public function create(): View
    {
        return view('admin.news.create');
    }

    /**
     * @param NewsRequest $request
     * @return View
     */
    public function store(NewsRequest $request): View
    {
        $news = new News([
            'title'        => $request->get('title_en'),
            'short_desc'   => $request->get('short_desc_en'),
            'body'         => $request->get('body_en'),
            'show_on_main' => $request->get('show_on_main') ? 1 : 0,
            'author_id'    => Auth::user()->id,
        ]);
        $news->save();

        if ($image = $request->file('image')) {
            foreach ($image as $files) {
                $destinationPath = 'media/news/'; // upload path
                if (!$files->move($destinationPath, $files->getClientOriginalName())) {
                    return abort(500);
                }
                $form = new Media([
                    'model_type'      => 'News',
                    'model_id'        => $news->id,
                    'collection_name' => 'news_image',
                    'file_name'       => $files->getClientOriginalName(),

                ]);
                $form->save();
            }
        }

        $news_local_title = new Localized([
            'tbl_name'        => 'news',
            'tbl_column_name' => 'title',
            'idx'             => $news->id,
            'ru'              => $request->get('title_ru'),
            'az'              => $request->get('title_az'),
        ]);

        $news_local_short_desc = new Localized([
            'tbl_name'        => 'news',
            'tbl_column_name' => 'short_desc',
            'idx'             => $news->id,
            'ru'              => $request->get('short_desc_ru'),
            'az'              => $request->get('short_desc_az'),
        ]);

        $news_local_body = new Localized([
            'tbl_name'        => 'news',
            'tbl_column_name' => 'body',
            'idx'             => $news->id,
            'ru'              => $request->get('body_ru'),
            'az'              => $request->get('body_az'),
        ]);
        $news_local_body->save();
        $news_local_short_desc->save();
        $news_local_title->save();

        return back()->with('success', 'News has been added');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param NewsRequest $request
     * @param int $id
     * @return void
     */
    public function update(NewsRequest $request, int $id)
    {

        $news = News::findOrFail($id)->with('localizedText')->where('id', $id)->firstOrFail();
        $news->title = $request->get('title_en');
        $news->short_desc = $request->get('short_desc_en');
        $news->body = $request->get('body_en');
        if ($request->get('show_on_main') == 1 || $news->show_on_main == 0)
            $news->published_at = Carbon::now();
        $news->show_on_main = $request->get('show_on_main') ? 1 : 0;
        $news->save();

        Localized::updateOrInsert(['idx' => $news->id, 'tbl_column_name' => 'title'], [
            'tbl_name'        => 'news',
            'tbl_column_name' => 'title',
            'az'              => $request->get('title_az'),
            'ru'              => $request->get('title_ru'),
        ]);

        Localized::updateOrInsert(['idx' => $news->id, 'tbl_column_name' => 'body'], [
            'tbl_name'        => 'news',
            'tbl_column_name' => 'body',
            'az'              => $request->get('body_az'),
            'ru'              => $request->get('body_ru'),
        ]);

        Localized::updateOrInsert(['idx' => $news->id, 'tbl_column_name' => 'short_desc'], [
            'tbl_name'        => 'news',
            'tbl_column_name' => 'short_desc',
            'az'              => $request->get('short_desc_az'),
            'ru'              => $request->get('short_desc_ru'),
        ]);

        return back()->with('success', 'News has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return void
     */
    public function destroy(int $id)
    {
        $news = News::findorfail($id);
        if ($news->status == 0) {
            $news->status = '1';
        } elseif ($news->status == 1) {
            $news->status = '0';
        }
        $news->save();
        return back();
    }
}
