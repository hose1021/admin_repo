<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\ReviewRequest;
use App\Models\Localized;
use App\Models\Review;
use Illuminate\Http\Response;

class ReviewController extends Controller
{
    /**
     * ReviewController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth', ['only' => ['index', 'edit', 'update', 'store', 'destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $reviews = Review::paginate(10);
        return view('admin.review.list', compact('reviews'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.review.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ReviewRequest $request
     * @return Response
     */
    public function store(ReviewRequest $request)
    {
        $review = new Review([
            'designator' => $request->get('designator_en'),
            'position'   => $request->get('position_en'),
            'review'     => $request->get('review_en'),
            'datetime'   => $request->get('datetime'),
        ]);
        $review->save();

        $news_local_title = new Localized([
            'tbl_name'        => 'reviews',
            'tbl_column_name' => 'designator',
            'idx'             => $review->id,
            'ru'              => $request->get('designator_ru'),
            'az'              => $request->get('designator_az'),
        ]);

        $news_local_short_desc = new Localized([
            'tbl_name'        => 'reviews',
            'tbl_column_name' => 'position',
            'idx'             => $review->id,
            'ru'              => $request->get('position_ru'),
            'az'              => $request->get('position_az'),
        ]);

        $news_local_body = new Localized([
            'tbl_name'        => 'reviews',
            'tbl_column_name' => 'review',
            'idx'             => $review->id,
            'ru'              => $request->get('review_ru'),
            'az'              => $request->get('review_az'),
        ]);
        $news_local_body->save();
        $news_local_short_desc->save();
        $news_local_title->save();

        return back()->with('success', 'Stock has been added');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function edit(int $id)
    {
        $review = Review::FindorFail($id)->with('localizedText')->where('id', $id)->firstOrFail();
        return view('admin.review.edit', compact('review'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param ReviewRequest $request
     * @param int $id
     * @return Response
     */
    public function update(ReviewRequest $request, int $id)
    {
        $review = Review::findOrFail($id)->with('localizedText')->where('id', $id)->firstOrFail();
        $review->designator = $request->get('designator_en');
        $review->position = $request->get('position_en');
        $review->review = $request->get('review_en');
        $review->datetime = $request->get('datetime');
        $review->save();

        Localized::updateOrInsert(['idx' => $review->id, 'tbl_column_name' => 'designator'], [
            'tbl_name'        => 'reviews',
            'tbl_column_name' => 'designator',
            'az'              => $request->get('designator_az'),
            'ru'              => $request->get('designator_ru'),

        ]);

        Localized::updateOrInsert(['idx' => $review->id, 'tbl_column_name' => 'position'], [
            'tbl_name'        => 'reviews',
            'tbl_column_name' => 'position',
            'az'              => $request->get('position_az'),
            'ru'              => $request->get('position_ru'),

        ]);

        Localized::updateOrInsert(['idx' => $review->id, 'tbl_column_name' => 'review'], [
            'tbl_name'        => 'reviews',
            'tbl_column_name' => 'review',
            'az'              => $request->get('review_az'),
            'ru'              => $request->get('review_ru'),

        ]);

        return back()->with('success', 'Stock has been added');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function destroy(int $id)
    {
        $review = Review::findorfail($id);
        if ($review->status == 0) {
            $review->status = '1';
        } elseif ($review->status == 1) {
            $review->status = '0';
        }
        $review->save();
        return back();
    }
}
