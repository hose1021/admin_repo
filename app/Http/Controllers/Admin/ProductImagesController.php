<?php

namespace App\Http\Controllers\Admin;

use App\Models\Media;
use Illuminate\Http\RedirectResponse;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ProductImagesController extends Controller
{
    /**
     * @param Request $request
     * @param int $id
     * @return string
     */
    public function upload(Request $request, int $id)
    {
        if ($image = $request->file()) {
            foreach ($image as $files) {
                $destinationPath = 'media/product/'; // upload path
                if (!$files->move($destinationPath, $files->getClientOriginalName())) {
                    return 'Error saving the file.';
                }
                $form = new Media([
                    'model_type'      => 'Products',
                    'model_id'        => $id,
                    'collection_name' => 'product_image',
                    'file_name'       => $files->getClientOriginalName(),

                ]);
                $form->save();
            }
        }
    }

    /**
     * @param int $image_id
     * @return RedirectResponse
     * @throws \Exception
     */
    public function destroy(int $image_id)
    {
        $media = Media::find($image_id);
        $media->delete();
        return back()->with('success', 'Image has been deleted');
    }
}
