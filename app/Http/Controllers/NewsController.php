<?php

namespace App\Http\Controllers;

use App\Models\News;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;

class NewsController extends Controller
{

    /**
     * @return Factory|View
     */
    public function index(): View
    {
        $newsList = News::paginate(6);
        return view('pages.news.news-list', compact('newsList'));
    }

    /**
     * @param int $id
     * @return Factory|View
     */
    public function show(int $id): View
    {
        $news = News::FindorFail($id);
        return view('pages.news.news-show', compact('news'));
    }
}
