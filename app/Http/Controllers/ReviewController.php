<?php

namespace App\Http\Controllers;

use App\Models\Review;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;

class ReviewController extends Controller
{
    /**
     * @return Factory|View
     */
    public function index(): View
    {
        $reviews = Review::paginate(2);
        return view('pages.reviews', compact('reviews'));
    }
}
