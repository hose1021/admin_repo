<?php

namespace App\Http\Controllers;

use App\Http\Requests\SendEmailRequest;
use App\Mail\ContactUs;
use Illuminate\Contracts\View\Factory;
use Illuminate\Support\Facades\Mail;
use Illuminate\View\View;

class ContactController extends Controller
{
    /**
     * @return Factory|View
     */
    public function index(): View
    {
        return view('pages.contacts');
    }

    public function sendEmail(SendEmailRequest $sendEmailRequest)
    {
        $validated = $sendEmailRequest->validated();

        Mail::send(new ContactUs($validated));

        return redirect()
            ->back()
            ->with('status', 'Thanks, your email has been successfully sent to us');
    }
}
