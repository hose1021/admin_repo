<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use App\Models\News;
use App\Models\Review;
use Illuminate\Contracts\Support\Renderable;
use App\User;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return Renderable
     */
    public function index()
    {
        $data = [
            'reviews'   => Review::get()->take(2),
            'lastNews'  => News::get()->take(5),
            'customers' => Customer::get()->take(3),
        ];
        return view('welcome', compact('data'));
    }
}
