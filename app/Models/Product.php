<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Support\Carbon;

/**
 * App\Models\ProductRequest
 *
 * @property int $id
 * @property string $name
 * @property string $images
 * @property string $short_desc
 * @property string $body
 * @property string $status
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|Product newModelQuery()
 * @method static Builder|Product newQuery()
 * @method static Builder|Product query()
 * @method static Builder|Product whereBody($value)
 * @method static Builder|Product whereCreatedAt($value)
 * @method static Builder|Product whereId($value)
 * @method static Builder|Product whereImages($value)
 * @method static Builder|Product whereName($value)
 * @method static Builder|Product whereShortDesc($value)
 * @method static Builder|Product whereStatus($value)
 * @method static Builder|Product whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read Collection|\App\Models\Localized[] $localizedText
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Media[] $media
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Media[] $productImage
 * @property-read \App\Models\Media $productMainImage
 */
class Product extends Model
{
    /**
     * @var string
     */
    protected $table = "products";

    /**
     * @var array
     */
    protected $fillable = ['name', 'short_desc', 'body', 'status'];

    /**
     * @return HasMany
     */
    public function localizedText(): HasMany
    {
        return $this->hasMany(Localized::class, 'idx', 'id')->where('tbl_name', 'products');
    }

    /**
     * @return HasMany
     */
    public function productImage(): HasMany
    {
        return $this->HasMany(Media::class, 'model_id', 'id')->where('model_type', 'Products')
            ->where('collection_name', 'product_image');
    }

    /**
     * @return HasOne
     */
    public function productMainImage(): HasOne
    {
        return $this->HasOne(Media::class, 'model_id', 'id')->where('model_type', 'Products')
            ->where('collection_name', 'product_main_image');
    }

    /**
     * @param $lang
     * @return string|null
     */
    public function name($lang): ?string
    {
        switch ($lang) {
            case "az":
                $name = $this->localizedText->where('tbl_column_name', 'title')->first();
                if ($name != NULL)
                    return $name->az;
                else break;
            case "ru":
                $name = $this->localizedText->where('tbl_column_name', 'title')->first();
                if ($name != NULL)
                    return $name->ru;
                else break;
        }
        return null;
    }

    /**
     * @param $lang
     * @return string|null
     */
    public function body($lang): ?string
    {
        switch ($lang) {
            case "az":
                $body = $this->localizedText->where('tbl_column_name', 'body')->first();
                if ($body != NULL)
                    return $body->az;
                else break;
            case "ru":
                $body = $this->localizedText->where('tbl_column_name', 'body')->first();
                if ($body != NULL)
                    return $body->ru;
                else break;
        }
        return null;
    }

    /**
     * @param $lang
     * @return string|null
     */
    public function short_desc($lang): ?string
    {
        switch ($lang) {
            case "az":
                $short_desc = $this->localizedText->where('tbl_column_name', 'short_desc')->first();
                if ($short_desc != NULL)
                    return $short_desc->az;
                else break;
            case "ru":
                $short_desc = $this->localizedText->where('tbl_column_name', 'short_desc')->first();
                if ($short_desc != NULL)
                    return $short_desc->ru;
                else break;
        }
        return null;
    }
}
