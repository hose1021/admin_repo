<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * App\Models\Media
 *
 * @property int $id
 * @property string $model_type
 * @property int $model_id
 * @property string $collection_name
 * @property string $name
 * @property string $file_name
 * @property string|null $mime_type
 * @property int $size
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static Builder|Media newModelQuery()
 * @method static Builder|Media newQuery()
 * @method static Builder|Media query()
 * @method static Builder|Media whereCollectionName($value)
 * @method static Builder|Media whereCreatedAt($value)
 * @method static Builder|Media whereFileName($value)
 * @method static Builder|Media whereId($value)
 * @method static Builder|Media whereMimeType($value)
 * @method static Builder|Media whereModelId($value)
 * @method static Builder|Media whereModelType($value)
 * @method static Builder|Media whereName($value)
 * @method static Builder|Media whereSize($value)
 * @method static Builder|Media whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property string $email
 * @property string $message
 * @property string $datetime
 * @property int|null $status
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Localized[] $localizedText
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Email whereDatetime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Email whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Email whereMessage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Email whereStatus($value)
 */
class Email extends Model
{
    /**
     * @var string
     */
    protected $table = "contacts_email";

    /**
     * @var array
     */
    protected $fillable = ['name', 'email', 'message', 'datetime', 'status'];


    /**
     * @return HasMany
     */
    public function localizedText(): HasMany
    {
        return $this->hasMany(Localized::class, 'idx', 'id')->where('tbl_name', 'contacts_email');
    }

    /**
     * @param $lang
     * @return string|null
     */
    public function name($lang): ?string
    {
        switch ($lang) {
            case "az":
                $name = $this->localizedText->where('tbl_column_name', 'name')->first();
                if ($name != NULL)
                    return $name->az;
                else break;
            case "ru":
                $name = $this->localizedText->where('tbl_column_name', 'name')->first();
                if ($name != NULL)
                    return $name->ru;
                else break;
        }
        return null;
    }

    /**
     * @param $lang
     * @return string|null
     */
    public function email($lang): ?string
    {
        switch ($lang) {
            case "az":
                $email = $this->localizedText->where('tbl_column_name', 'email')->first();
                if ($email != NULL)
                    return $email->az;
                else break;
            case "ru":
                $email = $this->localizedText->where('tbl_column_name', 'email')->first();
                if ($email != NULL)
                    return $email->ru;
                else break;
        }
        return null;
    }

    /**
     * @param $lang
     * @return string|null
     */
    public function message($lang): ?string
    {
        switch ($lang) {
            case "az":
                $message = $this->localizedText->where('tbl_column_name', 'message')->first();
                if ($message != NULL)
                    return $message->az;
                else break;
            case "ru":
                $message = $this->localizedText->where('tbl_column_name', 'message')->first();
                if ($message != NULL)
                    return $message->ru;
                else break;
        }
        return null;
    }
}
