<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Carbon;

/**
 * App\Models\Review
 *
 * @property int $id
 * @property string $designator
 * @property string $position
 * @property string $review
 * @property string $data_time
 * @property string $status
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|Review newModelQuery()
 * @method static Builder|Review newQuery()
 * @method static Builder|Review query()
 * @method static Builder|Review whereCreatedAt($value)
 * @method static Builder|Review whereDataTime($value)
 * @method static Builder|Review whereDesignator($value)
 * @method static Builder|Review whereId($value)
 * @method static Builder|Review wherePosition($value)
 * @method static Builder|Review whereReview($value)
 * @method static Builder|Review whereStatus($value)
 * @method static Builder|Review whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property string|null $datetime
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Localized[] $localizedText
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Review whereDatetime($value)
 */
class Review extends Model
{
    /**
     * @var string
     */
    protected $table = "reviews";

    /**
     * @var array
     */
    protected $fillable = ['designator', 'position', 'review', 'datetime', 'status'];

    /**
     * @return HasMany
     */
    public function localizedText()
    {
        return $this->hasMany(Localized::class, 'idx', 'id')->where('tbl_name', 'reviews');
    }

    /**
     * @param $lang
     * @return string|null
     */
    public function designator($lang): ?string
    {
        switch ($lang) {
            case "az":
                $designator = $this->localizedText->where('tbl_column_name', 'designator')->first();
                if ($designator != NULL)
                    return $designator->az;
                else break;
            case "ru":
                $designator = $this->localizedText->where('tbl_column_name', 'designator')->first();
                if ($designator != NULL)
                    return $designator->ru;
                else break;
        }
        return null;
    }

    /**
     * @param $lang
     * @return string|null
     */
    public function review($lang): ?string
    {
        switch ($lang) {
            case "az":
                $review = $this->localizedText->where('tbl_column_name', 'review')->first();
                if ($review != NULL)
                    return $review->az;
                else break;
            case "ru":
                $review = $this->localizedText->where('tbl_column_name', 'review')->first();
                if ($review != NULL)
                    return $review->ru;
                else break;
        }
        return null;
    }

    /**
     * @param $lang
     * @return string|null
     */
    public function position($lang): ?string
    {
        switch ($lang) {
            case "az":
                $position = $this->localizedText->where('tbl_column_name', 'position')->first();
                if ($position != NULL)
                    return $position->az;
                else break;
            case "ru":
                $position = $this->localizedText->where('tbl_column_name', 'position')->first();
                if ($position != NULL)
                    return $position->ru;
                else break;
        }
        return null;
    }
}
