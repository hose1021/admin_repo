<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Carbon;

/**
 * App\Models\News
 *
 * @property int $id
 * @property string|null $title
 * @property string|null $short_desc
 * @property string|null $body
 * @property int $author_id
 * @property string|null $main_active
 * @property int $status
 * @property Carbon $published_at
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read Collection|Localized[] $localizedText
 * @property-read Collection|Media[] $media
 * @property-read User $user
 * @method static Builder|News newModelQuery()
 * @method static Builder|News newQuery()
 * @method static Builder|News query()
 * @method static Builder|News whereAuthorId($value)
 * @method static Builder|News whereBody($value)
 * @method static Builder|News whereCreatedAt($value)
 * @method static Builder|News whereId($value)
 * @method static Builder|News whereMainActive($value)
 * @method static Builder|News wherePublishedAt($value)
 * @method static Builder|News whereShortDesc($value)
 * @method static Builder|News whereStatus($value)
 * @method static Builder|News whereTitle($value)
 * @method static Builder|News whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int $show_on_main
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News whereShowOnMain($value)
 */
class News extends Model
{
    /**
     * @var string
     */
    protected $table = 'news';

    /**
     * @var array
     */
    protected $fillable = [
        'title', 'short_desc', 'body', 'lang', 'author_id', 'show_on_main'
    ];

    /**
     * @var array
     */
    protected $dates = ['published_at', 'created_at', 'updated_at'];

    /**
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'author_id')->withDefault();
    }

    /**
     * @return HasMany
     */
    public function media(): HasMany
    {
        return $this->hasMany(Media::class, 'model_id', 'id')->where('model_type', 'News');
    }

    /**
     * @return HasMany
     */
    public function newsImage(): HasMany
    {
        return $this->HasMany(Media::class, 'model_id', 'id')->where('model_type', 'News')
            ->where('collection_name', 'product_image');
    }

    /**
     * @return HasMany
     */
    public function localizedText(): HasMany
    {
        return $this->hasMany(Localized::class, 'idx', 'id')->where('tbl_name', 'news');
    }

    /**
     * @param $lang
     * @return string|null
     */
    public function title($lang): ?string
    {
        switch ($lang) {
            case "az":
                $title = $this->localizedText->where('tbl_column_name', 'title')->first();
                if ($title != NULL)
                    return $title->az;
                else break;
            case "ru":
                $title = $this->localizedText->where('tbl_column_name', 'title')->first();
                if ($title != NULL)
                    return $title->ru;
                else break;
        }
        return null;
    }

    /**
     * @param $lang
     * @return string|null
     */
    public function body($lang): ?string
    {
        switch ($lang) {
            case "az":
                $body = $this->localizedText->where('tbl_column_name', 'body')->first();
                if ($body != NULL)
                    return $body->az;
                else break;
            case "ru":
                $body = $this->localizedText->where('tbl_column_name', 'body')->first();
                if ($body != NULL)
                    return $body->ru;
                else break;
        }
        return null;
    }

    /**
     * @param $lang
     * @return string|null
     */
    public function short_desc($lang): ?string
    {
        switch ($lang) {
            case "az":
                $short_desc = $this->localizedText->where('tbl_column_name', 'short_desc')->first();
                if ($short_desc != NULL)
                    return $short_desc->az;
                else break;
            case "ru":
                $short_desc = $this->localizedText->where('tbl_column_name', 'short_desc')->first();
                if ($short_desc != NULL)
                    return $short_desc->ru;
                else break;
        }
        return null;
    }
}
