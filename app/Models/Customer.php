<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Support\Carbon;

/**
 * App\Models\Customer
 *
 * @method static Builder|Customer newModelQuery()
 * @method static Builder|Customer newQuery()
 * @method static Builder|Customer query()
 * @mixin \Eloquent
 * @property int $id
 * @property string|null $name
 * @property float|null $latitude
 * @property float|null $longitude
 * @property string $description
 * @property int $status
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read Collection|Product[] $product
 * @method static Builder|Customer whereCreatedAt($value)
 * @method static Builder|Customer whereDescription($value)
 * @method static Builder|Customer whereId($value)
 * @method static Builder|Customer whereLatitude($value)
 * @method static Builder|Customer whereLongitude($value)
 * @method static Builder|Customer whereName($value)
 * @method static Builder|Customer whereStatus($value)
 * @method static Builder|Customer whereUpdatedAt($value)
 */
class Customer extends Model
{
    /**
     * @var string
     */
    protected $table = 'customers';

    /**
     * @var array
     */
    protected $fillable = ['name', 'latitude', 'longitude', 'description'];

    /**
     * @return BelongsToMany
     */
    public function product(): BelongsToMany
    {
        return $this->belongsToMany(Product::class, 'customer_products', 'cus_id', 'prdct_id');
    }

}
