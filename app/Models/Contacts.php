<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * App\Models\Contacts
 *
 * @property int $id
 * @property string|null $address
 * @property string|null $phone
 * @property string|null $fax
 * @property string|null $emails
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|Contacts newModelQuery()
 * @method static Builder|Contacts newQuery()
 * @method static Builder|Contacts query()
 * @method static Builder|Contacts whereAddress($value)
 * @method static Builder|Contacts whereCreatedAt($value)
 * @method static Builder|Contacts whereEmails($value)
 * @method static Builder|Contacts whereFax($value)
 * @method static Builder|Contacts whereId($value)
 * @method static Builder|Contacts wherePhone($value)
 * @method static Builder|Contacts whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int $status
 * @method static Builder|Contacts whereStatus($value)
 */
class Contacts extends Model
{
    /**
     * @var string
     */
    protected $table = 'contacts';

    /**
     * @var array
     */
    protected $fillable = ['address', 'phone', 'fax', 'emails', 'status'];
}
