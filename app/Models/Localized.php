<?php declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;


/**
 * App\Models\Localized
 *
 * @property int $id
 * @property string|null $tbl_name
 * @property string|null $tbl_column_name
 * @property int|null $idx
 * @property string|null $ru
 * @property string|null $az
 * @property-read \App\Models\News|null $post
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Localized newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Localized newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Localized query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Localized whereAz($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Localized whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Localized whereIdx($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Localized whereRu($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Localized whereTblColumnName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Localized whereTblName($value)
 * @mixin \Eloquent
 * @property-read \App\Models\News|null $news
 * @property-read \App\Models\Product|null $products
 * @property-read \App\Models\Review|null $reviews
 */
class Localized extends Model
{
    /**
     * @var string
     */
    protected $table ="localized_texts";

    /**
     * @var array
     */
    protected $fillable = ['tbl_name', 'tbl_column_name', 'idx', 'az', 'ru'];

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @return BelongsTo
     */
    public function news(): BelongsTo
    {
        return $this->belongsTo(News::class,'idx')->withDefault();
    }

    /**
     * @return BelongsTo
     */
    public function products(): BelongsTo
    {
        return $this->belongsTo(Product::class,'idx')->withDefault();
    }

    /**
     * @return BelongsTo
     */
    public function reviews(): BelongsTo
    {
        return $this->belongsTo(Review::class,'idx')->withDefault();
    }
}
